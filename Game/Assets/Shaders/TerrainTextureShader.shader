﻿Shader "Custom/TerrainTextureShader"
{
    Properties
    {
		_GrassTex("Grass Texture", 2D) = "white"{}
		_RockTex("Rock Texture", 2D) = "white"{}
		_SnowTex("Snow Texture", 2D) = "white"{}
		_PersistentFogTex("Persistent Fog of War Texture", 2D) = "white"{}
		_ExtinctFogTex("Extinct Fog of War Texture", 2D) = "white"{}
		_RoamingFogTex("Roaming Fog of War Texture", 2D) = "white"{}
		_NoiseTex("Noise Texture", 2D) = "white"{}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_L("Light Vector", Vector) = (1.0, 1.0, 1.0, 1.0)
		_SnowElevations("Snow Elevations", Vector) = (1.0, 1.0, 1.0, 1.0)
		_CamPosWorld("Cam Pos World", Vector) = (1.0, 1.0, 1.0, 1.0)
		_TerrainSpan("Terrain Span", Float) = 1.0
		_FogAltitudeCoefs("Fog Altitude Coefs", Vector) = (1.0, 1.0, 1.0, 1.0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows finalcolor:fogOfWarModifier

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.5

		#include "UnityCG.cginc"

		sampler2D _GrassTex;
		sampler2D _RockTex;
		sampler2D _SnowTex;
		sampler2D _NoiseTex;
		sampler2D _PersistentFogTex;
		sampler2D _ExtinctFogTex;
		sampler2D _RoamingFogTex;
		sampler2D _CameraDepthTexture;

		half _Glossiness;
		half _Metallic;
		float3 _L;
		float2 _SnowElevations;
		float3 _CamPosWorld;
		float	_TerrainSpan;
		float4  _FogAltitudeCoefs;

//		const float2x2 M2 = float2x2(0.8, -0.6, 0.6, 0.8);
//		float rndPerlin(float2 p)
//		{
//			float f = 0.0;
//			f += 0.5 * tex2D(_NoiseTex, p / 256.0).r; p = M2 * p * 2.02;
//			f += 0.25 * tex2D(_NoiseTex, p / 256.0).r; p = M2 * p * 2.03;
//			f += 0.125 * tex2D(_NoiseTex, p / 256.0).r; p = M2 * p * 2.01;
//			f += 0.0625 * tex2D(_NoiseTex, p / 256.0).r;
//			return f / 0.9375;
//		}

		float3 textureBlend3(float4 tex1, float a1, float4 tex2, float a2, float4 tex3, float a3)
		{
			return tex1.xyz * a1 + tex2.xyz * a2 + tex3.xyz * a3;
		}

        struct Input
        {
            float2 uv_GrassTex;
			float2 uv_RockTex;
			float2 uv_SnowTex;
			float2 uv_NoiseTex;
			float2 uv_PersistentFogTex;
			float2 uv_ExtinctFogTex;
			float2 uv_RoamingFogTex;
			float3 worldNormal;
			float3 worldPos;
			float3 viewDir;
			float4 screenPos;
        };

        

        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

		void fogOfWarModifier(Input IN, SurfaceOutputStandard o, inout fixed4 color)
		{
//			float2 uv = IN.screenPos.xy / IN.screenPos.w;
//			float depth = 2.0 * Linear01Depth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv));

			float3 viewVec = IN.worldPos - _CamPosWorld;
			float dist = length(viewVec);
			float normDist = dist / _TerrainSpan;

//			float fogAmt = 1.0 - exp(-normDist);
			viewVec = normalize(viewVec);
			float c = _FogAltitudeCoefs.y;
			float b = _FogAltitudeCoefs.x;
			float alt = _FogAltitudeCoefs.z;
			float otherFogAmt = c * (2.8 + (1.0 - exp((_CamPosWorld.y / 4400.0) * b))) * (1.0 - exp(-normDist * viewVec.y * b)) / viewVec.y;
			float fogAmt = exp(-IN.worldPos.y / alt);
			float totalFog = max(fogAmt, otherFogAmt);

			float sunAng = max(dot(normalize(-_L), normalize(viewVec)), 0.0);
			float3 fogColor = lerp( float3(0.5, 0.6, 0.7), float3(1.0, 0.9, 0.7), pow(sunAng, 8.0));

			#ifdef UNITY_PASS_FORWARDADD
			fogColor = 0;
			#endif

			float3 adjColor = lerp(color, fogColor, otherFogAmt);

			float persistentFOW = tex2D(_PersistentFogTex, IN.uv_PersistentFogTex).r;
			float extinctFOW = tex2D(_ExtinctFogTex, IN.uv_ExtinctFogTex).r;
			float roamingFOW = tex2D(_RoamingFogTex, IN.uv_RoamingFogTex).r;
			float fow = 0.0;
			fow = max(persistentFOW, (max(roamingFOW, max(extinctFOW, 0.0))));
			color = float4(adjColor, color.w) * fow;
//			color = color * fow;
		}

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float4 grassSample = tex2D(_GrassTex, IN.uv_GrassTex);
			float4 rockSample = tex2D(_RockTex, IN.uv_RockTex);
			float4 snowSample = tex2D(_SnowTex, IN.uv_SnowTex);
//			float persistentFOW = tex2D(_PersistentFogTex, IN.uv_PersistentFogTex).r;
//			float extinctFOW = tex2D(_ExtinctFogTex, IN.uv_ExtinctFogTex).r;
//			float roamingFOW = tex2D(_RoamingFogTex, IN.uv_RoamingFogTex).r;
//			float fow = 0.0;
//			fow = max(persistentFOW,(max(roamingFOW, max(extinctFOW, 0.0))));

			float3 N = IN.worldNormal;

			// Terrain land type //
			float dpy = dot(N, float3(0.0, 1.0, 0.0));
			float normalRamp = lerp(0.0, 1.0, dpy);
			float rock = 1.0 - pow(normalRamp, 1.5);

			float ge = smoothstep(0.0, 3000.0, IN.worldPos.y);      // 0 - 3000
			float grassSlope = pow(clamp(dot(N, float3(0.0, 1.0, 0.0)), 0.0, 1.0), 4.0);
			float grass = min(grassSlope, 1.0 - ge);


			/////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////
			float3 P = IN.worldPos.xyz;
			float3 V = IN.viewDir;
			float3 L = normalize(_L);
//			float2 meshUV = (vec2(P.xz) - meshMins) / terrainSpan;

			float fresnel = clamp(1.0 + dot(V, N), 0.0, 1.0);
//			float3 refl = reflect(V, N);
//			float vis = 1.0;						// shadow

			float3 rgb = float3(0.0, 0.0, 0.0);
//			float globalAlpha = 1.0;

			float r = tex2D(_NoiseTex, P.xz / 2048.0).r;

			rgb = (r * 0.25 + 0.75) * 1.5 * lerp(float3(0.08, 0.05, 0.03), float3(0.1, 0.09, 0.08),
					tex2D(_NoiseTex, 0.00001 * float2(P.x, P.y * 48.0)).r);
			rgb = lerp(rgb, 0.2 * float3(0.45, 0.3, 0.15) * (0.5 + 0.5 * r), smoothstep(0.7, 0.9, N.y));
			rgb = lerp(rgb, 0.15 * float3(0.3, 0.3, 0.1) * (0.25 * 0.75 * r), smoothstep(0.95, 1.0, N.y));

			// snow //
			float h = smoothstep(_SnowElevations.x, _SnowElevations.y, P.y + 500.0);// * rndPerlin(P.xz));
			float e = smoothstep(1.0 - 0.5 * h, 1.0 - 0.1 * h, N.y);
			float thisO = 0.3 + 0.7 * smoothstep(0.0, 0.1, N.x + h * h);
			float s = h * e * thisO;
			rgb = lerp(rgb, 1.29 * float3(0.62, 0.65, 0.7), smoothstep(0.1, 0.9, s));

			// gamma 
			rgb = pow(rgb, float3(1.0, 1.0, 1.0));

			// Dither
			float tmp;
			tmp = lerp(-0.5 / 255.0, 0.5 / 255.0, tex2D(_NoiseTex, IN.uv_NoiseTex * float2(384.0, 384.0)).r);
			rgb += float3(tmp, tmp, tmp);

//			vec4 rockTexSample = texture(rockTexture, meshUV * 1024.0);		// * rockTexRepeat
//			vec4 grassTexSample = texture(grassTexture, meshUV * 1024.0);	// * grassTexRepeat
//			vec4 snowTexSample = texture(snowTexture, meshUV * 1024.0);

			
			float3 detailSample = textureBlend3(grassSample, grass, rockSample, rock, snowSample, s);
			rgb = lerp(rgb, detailSample.rgb, 0.4);

			////////////////////////////////////////////////////////////////////////
			///////////////////////////////////////////////////////////////////////

			float roughness = 1.0;
			float metal = 0.0;
			float ao = 1.0;

//			color0 = vec4(outPositionGS.xyz, 1.0);
//			color1 = vec4(N, 0.0);
//			color2 = vec4(rgb, roughness);
//			color3 = vec4(shadow, metal, ao, 1.0);

			if(s > 0.05)
			{
				metal = 0.1;
				roughness = 0.7;
			}	

            o.Metallic = 0.0;
            o.Smoothness = 0.0;	
			o.Alpha = 1.0;
			o.Albedo = rgb;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
