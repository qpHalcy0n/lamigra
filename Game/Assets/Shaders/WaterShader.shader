﻿Shader "Custom/WaterShader"
{
    Properties
    {
		_Tess("Max Tessellation", Range(1, 16)) = 4
        _ReflectionMap("Reflection Map", 2D) = "white" {}
    }
 
    SubShader
    {
        Pass
        {
            Tags { "RenderType" = "Opaque" }
            LOD 200
 
            CGPROGRAM
            #pragma target 4.6
            #pragma vertex vert
            #pragma geometry geom  
            #pragma fragment frag
            #pragma hull hull
            #pragma domain domain
            
            #include "UnityCG.cginc"

            sampler2D _ReflectionMap;
			sampler2D _CameraGBufferTexture0;
			sampler2D _TerrainHeightmap;
			sampler2D _CameraDepthTexture;
			sampler2D _foamTexture;
			float4    _foamRanges;				// x: x, y: y, z: horizontal foam, w: foam depth
			float4	  _meshSpan;				// x: span x, y: 0.0, z: span z, w: max elevation
			float4    _frustumUL;
			float4    _frustumUR;
			float4    _frustumLL;
			float4    _frustumLR;
            float     _Tess;
			float4x4  _reflCamMVP;
			float4x4  _inverseV;
			float4    _waveParams;				// x: waveHeight, y: dt, z: dtDamping, w: water elevation
			float	  _refractionScale;
			float	  _distortion;
			float	  _radiance;
			float3	  _camPosWS;
			float3    _lightDirWS;
			float4	  _gerstner_direction[5];
			float	  _gerstner_amplitude[5];
			float	  _gerstner_steepness[5];
			float     _gerstner_frequency[5];
			float     _gerstner_speed[5];
			int		  _num_gerstner_waves;
			float2    _clarityParams;		// x: clarity, y: transparency
			float3    _shoreTint;
			float3    _surfaceColor;
			float3	  _deepColor;
			float3	  _horizontalExtinction;
 
            struct appdata
            {
                float4 vertex : POSITION;
                float4 tangent : TANGENT;
                float3 normal : NORMAL;
                float2 texcoord : TEXCOORD0;
            };

			struct PatchData
			{
				float4 vertex	: TEXCOORD0;
				float2 uv		: TEXCOORD1;
				float3 normal	: TEXCOORD2;
			};

            PatchData vert(appdata v)
            {
                PatchData o;
				o.vertex = v.vertex;
                o.uv = v.texcoord;
				o.normal = v.normal;

                return o;
            }
 
            struct TessellationFactors
            {
                float edge[3] : SV_TessFactor;
                float inside : SV_InsideTessFactor;
            };
 
            TessellationFactors patchConstantFunction(InputPatch<PatchData, 3> patch)
            {
                TessellationFactors f;
				f.edge[0] = _Tess;
				f.edge[1] = _Tess;
				f.edge[2] = _Tess;
				f.inside = _Tess;

                return f;
            }

            [UNITY_domain("tri")]
            [UNITY_outputcontrolpoints(3)]
            [UNITY_outputtopology("triangle_cw")]
            [UNITY_partitioning("integer")]
            [UNITY_patchconstantfunc("patchConstantFunction")]
            PatchData hull(InputPatch<PatchData, 3> patch, uint id : SV_OutputControlPointID)
            {
                return patch[id];
            }

			struct GeometryInput
			{
				float4 vertex : TEXCOORD0;
				float3 normal : TEXCOORD1;
			};
 
            [UNITY_domain("tri")]
            GeometryInput domain(TessellationFactors factors, OutputPatch<PatchData, 3> patch, float3 bary : SV_DomainLocation)
            {
				GeometryInput vOut;
                vOut.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				vOut.normal = patch[0].normal * bary.x + patch[1].normal * bary.y + patch[2].normal * bary.z;
			
                return vOut;
            }

			/////////////// geom //////////////////////////////
 
            struct GeometryOutput
            {
                float4 pos : SV_POSITION;
				float4 reflectionNDC	: TEXCOORD0;
				float3 normal			: TEXCOORD1;
				float3 wsPos			: TEXCOORD2;
            };

			float3 GerstnerWaveNormal(float3 pos, float time)
			{
				float3 N = float3(0.0, 1.0, 0.0);
				for (int i = 0; i < _num_gerstner_waves; i++)
				{
					float proj = dot(pos.xz, _gerstner_direction[i].xz);
					float phase = time * _gerstner_speed[i];
					float psi = proj * _gerstner_frequency[i] + phase;
					float Af = _gerstner_amplitude[i] * _gerstner_frequency[i];
					float alpha = Af * sin(psi);

					N.y -= _gerstner_steepness[i] * alpha;
					float x = _gerstner_direction[i].x;
					float z = _gerstner_direction[i].z;
					float omega = Af * cos(psi);

					N.x -= x * omega;
					N.z -= z * omega;
				}

				return N;
			}

			float3 GerstnerWavePosition(float2 pos, float time)
			{
				float3 P = float3(pos.x, 0.0, pos.y);
				for (int i = 0; i < _num_gerstner_waves; i++)
				{
					float proj = dot(pos, _gerstner_direction[i].xz);
					float phase = time * _gerstner_speed[i];
					float theta = proj * _gerstner_frequency[i] + phase;
					float height = _gerstner_amplitude[i] * sin(theta);

					P.y += height;

					float maxWidth = _gerstner_steepness[i] * _gerstner_amplitude[i];
					float width = maxWidth * cos(theta);
					float x = _gerstner_direction[i].x;
					float z = _gerstner_direction[i].z;

					P.x += x * width;
					P.z += z * width;
				}

				return P;
			}
			
			float3 GerstnerWave(float2 pos, float time, inout float3 normal)
			{
				float3 P = GerstnerWavePosition(pos, time);
				normal = GerstnerWaveNormal(P, time);
				return P;
			}
 
            [maxvertexcount(32)]
            void geom(triangle GeometryInput IN[3], inout TriangleStream<GeometryOutput> triStream)
            {
				// THESE VERTICES ARE IN FUCKING WORLD SPACE
				GeometryOutput o;
				float3 N;
				float dt = _waveParams.y;
				float waterElevation = _waveParams.w;

				float4 adjustedVert;
				adjustedVert = IN[0].vertex;
				adjustedVert.xyz = GerstnerWave(adjustedVert.xz, dt, N);
				adjustedVert.y += waterElevation;
                o.pos = UnityObjectToClipPos(adjustedVert);
				o.reflectionNDC = mul(_reflCamMVP, float4(adjustedVert.xyz, 1.0));
				o.normal = N;
				o.wsPos = adjustedVert;
                triStream.Append(o);
 
				adjustedVert = IN[1].vertex;
				adjustedVert.xyz = GerstnerWave(adjustedVert.xz, dt, N);
				adjustedVert.y += waterElevation;
                o.pos = UnityObjectToClipPos(adjustedVert);
				o.normal = N;
				o.reflectionNDC = mul(_reflCamMVP, float4(adjustedVert.xyz, 1.0));
				o.wsPos = adjustedVert;
                triStream.Append(o);
 
				adjustedVert = IN[2].vertex;
				adjustedVert.xyz = GerstnerWave(adjustedVert.xz, dt, N);
				adjustedVert.y += waterElevation;
                o.pos = UnityObjectToClipPos(adjustedVert);
				o.normal = N;
				o.reflectionNDC = mul(_reflCamMVP, float4(adjustedVert.xyz, 1.0));
				o.wsPos = adjustedVert;
                triStream.Append(o);
            }

			/////////////////////////// frag ///////////////////////////////////////////
			float3 getDepthRefractionColor(float2 waterTransparency, float2 waterDepthValues,
										   float shoreRange, float3 horizontalExtinction,
										   float3 refractionColor, float3 shoreColor, float3 surfaceColor,
										   float3 depthColor)
			{
				float waterClarity = waterTransparency.x;
				float visibility = waterTransparency.y;
				float waterDepth = waterDepthValues.x;          // vertical depth
				float viewWaterDepth = waterDepthValues.y;      // accum value

				float accDepth = viewWaterDepth * waterClarity;
				float accDepthExp = clamp(accDepth / (2.5 * visibility), 0.0, 1.0);
				accDepthExp *= (1.0 - accDepthExp) * accDepthExp * accDepthExp + 1.0;

				horizontalExtinction *= 0.08;       // 0.09
				surfaceColor = lerp(shoreColor, surfaceColor, clamp(waterDepth / shoreRange, 0.0, 1.0));
				float3 waterColor = lerp(surfaceColor, depthColor, clamp(waterDepth / horizontalExtinction, 0.0, 1.0));
				float3 final = lerp(refractionColor, surfaceColor * waterColor, clamp(accDepth / visibility, 0.0, 1.0));
				final = lerp(final, depthColor, accDepthExp);
				final = lerp(final, depthColor * waterColor, clamp(waterDepth / horizontalExtinction, 0.0, 1.0));

				return final;
			}
 
            float4 frag(GeometryOutput i) : SV_Target
            {
                float4 color = float4(1.0, 0.0, 0.0, 1.0);
				float3 N = normalize(i.normal);
				float waveAmplitude = 18.1;

				// reconstruct terrain elevation under water point
				float terrainX = max(0.0, i.wsPos.x / _meshSpan.x);
				float terrainZ = max(0.0, i.wsPos.z / _meshSpan.z);
				float heightSample = tex2D(_TerrainHeightmap, float2(terrainX, terrainZ)).r * 8800;
				float3 terrainPosS = float3(terrainX, heightSample, terrainZ);
				float waterElevation = _waveParams.w;
				float depth = (i.wsPos.y - heightSample) / (waterElevation + 0.5 * 18.1);
				float2 terrainUV = float2(terrainX, terrainZ);

				// for reflection UV
				float3 reflectionNormalized = i.reflectionNDC.xyz / i.reflectionNDC.w;
				float2 reflectionUV = reflectionNormalized.xy * 0.5 + 0.5;

				// main camera space UV
				float4 uv = i.pos;
				uv.xy /= _ScreenParams.xy;
		
				// Reconstruct terrain at view ray hit
				float depthSample = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, uv.xy);
				float camDepth = Linear01Depth(depthSample);

				float viewX = lerp(_frustumUL.x, _frustumUR.x, uv.x);
				float viewY = lerp(_frustumUL.y, _frustumLL.y, 1.0 - uv.y);
				float viewZ = -_frustumUL.z;
				float3 viewSpaceViewVec = float3(viewX, viewY, viewZ);
				float3 viewSpacePos = viewSpaceViewVec * camDepth;
				float4 worldSpacePos = mul(_inverseV, float4(viewSpacePos, 1.0));
				float4 worldSpaceViewVec;
				worldSpaceViewVec.xyz = worldSpacePos.xyz - _camPosWS;
				worldSpaceViewVec.w = 0.0;
				float accum = length(worldSpacePos.xyz - i.wsPos.xyz) / waterElevation;
				float dt = _waveParams.y;

				// Fresnel + refl
				float3 Vn = normalize(worldSpaceViewVec);
				float r0 = pow( (1.0 - 1.33333) / (1.0 + 1.3333), 2.0);
				float fresnel = r0 + (1.0 - r0) * pow(1.0 - dot(N, -Vn), 5.0);
				reflectionUV = reflectionUV + _distortion * N.xz;
				float3 skyReflectionColor = tex2D(_ReflectionMap, reflectionUV);
				
				// spec
				float spec = pow(saturate(dot(N, -Vn)), 0.35); // 0.2
				float3 specColor = spec * float3(2.8, 2.7, 2.0) * lerp(float3(0.0, 0.0, 0.0), skyReflectionColor, float3(fresnel, fresnel, fresnel));
				
				// scatter
				float minWaveHeight = waterElevation - (waveAmplitude * 0.5);
				float maxWaveHeight = waterElevation + (waveAmplitude * 0.5);
				float waveHeight = (i.wsPos.y - minWaveHeight) / (maxWaveHeight - minWaveHeight);
				float scatterFactor = saturate(dot(N, float3(_lightDirWS.x, 0.0, _lightDirWS.z))) * waveHeight;
				float3 deep = _deepColor;
				float3 crest = float3(0.2421875, 0.5078125, 0.48671875);
				float3 totalSpecular = lerp(deep, crest, scatterFactor) + specColor;

				// QUADRION REFLECTION
				float3 qReflectionColor = skyReflectionColor * fresnel * _radiance;

				// QUADRION REFRACTION
				float refractionScale = _refractionScale * min(depth, 1.0);
				float2 delta = float2(sin((dt * 0.1) + 0.1 * abs(i.wsPos.y)), sin((dt * 0.1) + 0.07 * abs(i.wsPos.y)));
				float2 refractUV = uv + refractionScale * delta;
				float3 refractionSample = tex2D(_CameraGBufferTexture0, refractUV).rgb;
				float2 waterDepthVals = float2(depth, accum);
				float shoreRange = max(_foamRanges.x, _foamRanges.y) * 2.0;
				float3 qRefractionColor = getDepthRefractionColor(_clarityParams, waterDepthVals, shoreRange,
																  _horizontalExtinction, refractionSample, 
																  _shoreTint, _surfaceColor, _deepColor);
				qRefractionColor += totalSpecular;

				// QUADRION FINAL
				float3 qFinalColor = lerp(qRefractionColor, qReflectionColor, fresnel * saturate(depth / _foamRanges.x * 0.4));

				// foam 
				float foamAmt = 0.0;
				float maxFoamDepth = _foamRanges.w;
				float waterDepth = (i.wsPos.y - heightSample);
				float3 foamMask = float3(0.0, 0.0, 0.0);
				float2 foamUV = terrainUV * 256.0;
				float2 foamUV0 = foamUV - float2(dt * 0.1, cos(4 * (1.0 - depth)));
				float2 foamUV1 = foamUV * 0.5 + float2(sin(4 * (1.0 - depth)), dt * 0.1);
				if(waterDepth <= maxFoamDepth)
				{
					float3 foamA = tex2D(_foamTexture, foamUV0).rgb;
					float3 foamB = tex2D(_foamTexture, foamUV1).rgb;
					foamMask = (foamA + foamB) * 0.45;
					foamMask = saturate(pow(foamMask, 2.0));
					foamAmt = 1.0 - smoothstep(0.0, 1.0, waterDepth / maxFoamDepth);
					foamAmt = lerp(0.0, foamAmt, waterDepth / maxFoamDepth);
				}	
				foamMask *= foamAmt;

				qFinalColor += foamMask;
				color = float4(qFinalColor, 1.0);
                return color;
            }
 
            ENDCG
        }
    }
}
