﻿Shader "Quadrion/Skybox"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_V("View Vector", Vector) = (1.0, 1.0, 1.0, 1.0)
		_L("Light Vector", Vector) = (1.0, 1.0, 1.0, 1.0)

		_rayleighBrightness("Rayleigh Brightness", Float) = 2.527
		_rayleighCollectionPower("Rayleigh Collection Power", Float) = 0.32591
		_rayleighStrength("Rayleigh Strength", Float) = 0.256
		_mieDistribution("Mie Distribution", Float) = 0.203
		_mieCollectionPower("Mie Collection Power", Float) = 0.989
		_mieStrength("Mie Strength", Float) = 0.3827
		_mieBrightness("Mie Brightness", Float) = 1.741
		_scatterStrength("Scatter Strength", Float) = 0.0803
		_surfaceHeight("Surface Height", Float) = 0.9848
		_faceID("Face ID", Int) = 0
    }

    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

			float4 _V;
			float4 _L;
			float4x4 _inverseP;
			float4x4 _inverseV;
			float _rayleighBrightness;
			float _rayleighCollectionPower;
			float _rayleighStrength;
			float _mieDistribution;
			float _mieCollectionPower;
			float _mieStrength;
			float _mieBrightness;
			float _scatterStrength;
			float _surfaceHeight;
			int	  _faceID;

			static const float spotBrightness = 80.0f;
			static const float intensity = 1.0f;
			static const float nSteps = 4.0f;
			static const float nIrradianceSamples = 128.0f;
			static const float nInvIrradianceSamples = 1.0f / nIrradianceSamples;
			static const float Pi = 3.141592654f;
			static const float3 Kr = float3(0.18867780436772762f, 0.4978442963618773f, 0.6616065586417131f);

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
//                float4 vertex : SV_POSITION;
				float3 P : TEXCOORD1;
				float3 L : TEXCOORD2;
            };

			float phase(float alpha, float g)
			{
				float a = 3.0 * (1.0 - g * g);
				float b = 2.0 * (2.0 + g * g);
				float c = 1.0 + alpha * alpha;
				float d = pow(1.0 + g * g - 2.0 * g * alpha, 1.5);
				return (a / b) * (c / d);
			}


			float atmospheric_depth(float3 pos, float3 dir)
			{
				float a = dot(dir, dir);
				float b = 2.0 * dot(dir, pos);
				float c = dot(pos, pos) - 1.0;
				float det = b * b - 4.0 * a * c;
				float detSqrt = sqrt(det);
				float q = (-b - detSqrt) / 2.0;
				float t1 = c / q;
				return t1;
			}

			float horizon_extinction(float3 pos, float3 dir, float rad)
			{
				float u = dot(dir, -pos);
				if (u < 0.0)
					return 1.0;

				float3 near = pos + u * dir;
				if (length(near) < rad)
					return 0.0;

				else
				{
					float3 v2 = normalize(near) * rad - pos;
					float diff = acos(dot(normalize(v2), dir));
					return smoothstep(0.0, 1.0, pow(diff * 2.0, 3.0));
				}
			}

			float3 absorb(float dist, float3 color, float factor)
			{
				float3 e = pow(Kr, float3(factor / dist, factor / dist, factor / dist));
				return float3(color - color * e);
			}

			float3 get_view_vector(v2f i, float4 screenCoords) : fragment
			{
				float2 texSize = float2(256.0, 256.0);

				float2 correctedSC = float2(screenCoords.x, screenCoords.y);
//				float2 fragCoord = correctedSC / _ScreenParams.xy;
				float2 fragCoord = correctedSC / texSize;
				fragCoord = (fragCoord - 0.5) * 2.0;
				float4 ndc = float4(fragCoord, 0.0, 1.0);
				float4 Veye = mul(_inverseP, ndc);
				float3 eye = normalize(Veye.xyz);
				float4 Vworld = mul(_inverseV, float4(eye, 0.0));
				float3 world = normalize(Vworld.xyz);

				return world;
			}

            v2f vert (appdata v, out float4 outpos : SV_POSITION)
            {
                v2f o;
                outpos = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

				o.P = float3(0.0, _surfaceHeight, 0.0);
				o.L = float3(-1.0, -1.0, -1.0) * normalize(_L);

                return o;
            }

            sampler2D _MainTex;

            fixed4 frag (v2f i, UNITY_VPOS_TYPE screenCoords : VPOS) : SV_Target
            {
				float3 V = get_view_vector(i, screenCoords);
				float alpha = dot(V, i.L);
				float rayleighFactor = phase(alpha, -0.01) * _rayleighBrightness;
				float mieFactor = phase(alpha, _mieDistribution) * _mieBrightness;
				float spot = smoothstep(0.0, 15.0, phase(alpha, 0.9995)) * spotBrightness;

				float eyeDepth = atmospheric_depth(i.P, V);
				float stepLen = eyeDepth / nSteps;

				float eyeExtinction = horizon_extinction(i.P, V, i.P.y - 0.05);			// 0.15

				float3 rayleighCollected = float3(0.0, 0.0, 0.0);
				float3 mieCollected = float3(0.0, 0.0, 0.0);

				for (int ii = 0; ii < nSteps; ii++)
				{
					float sampDist = stepLen * float(ii);
					float3 pos = i.P + V * sampDist;
					float extinction = horizon_extinction(pos, i.L, i.P.y - 0.25);		// 0.35
					float sampDepth = atmospheric_depth(pos, i.L);

					float3 influx = absorb(sampDepth, float3(intensity, intensity, intensity), _scatterStrength) * float3(extinction, extinction, extinction);

					rayleighCollected += absorb(sampDist, Kr * influx, _rayleighStrength);
					mieCollected += absorb(sampDist, influx, _mieStrength);
				}

				rayleighCollected = (rayleighCollected * eyeExtinction * pow(eyeDepth, _rayleighCollectionPower)) / float(nSteps);
				mieCollected = (mieCollected * eyeExtinction * pow(eyeDepth, _mieCollectionPower)) / float(nSteps);

				float3 color = float3(spot * mieCollected + mieFactor * mieCollected + rayleighFactor * rayleighCollected);

				fixed4 col = fixed4(color.x, color.y, color.z, 1.0);
                return col;
            }
            ENDCG
        }
    }
}
