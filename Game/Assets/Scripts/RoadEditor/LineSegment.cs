﻿using UnityEngine;

public class LineSegment
{
    public Vector3 Start { get; set; }
    public Vector3 End { get; set; }

    public bool Equals(LineSegment other)
    {
        return null != other && Start == other.Start && End == other.End;
    }
    public override bool Equals(object obj)
    {
        return Equals(obj as LineSegment);
    }

	public override int GetHashCode()
	{
        int hashCode = -1676728671;
        hashCode = hashCode * -1521134295 + Start.GetHashCode();
        hashCode = hashCode * -1521134295 + End.GetHashCode();
        return hashCode;
    }
}
