﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Tests
{
    public class LineTests
    {
        [Test]
        public void GenerateLineSegmentPoints()
        {
            var line = new Line(new Vector3(1, 0, 0), new Vector3(6, 0, 0), 1);
            var expectedPoints = new List<Vector3>
            {
                new Vector3(1, 0, 0),
                new Vector3(2, 0, 0),
                new Vector3(3, 0, 0),
                new Vector3(4, 0, 0),
                new Vector3(5, 0, 0),
                new Vector3(6, 0, 0)
            };
            
            var lineSegmentPoints = Line.GenerateLineSegmentPoints(line.Start, line.End, 1);

            Assert.AreEqual(lineSegmentPoints.Count, expectedPoints.Count);

            for (int i = 0; i < lineSegmentPoints.Count; ++i)
            {
                Assert.AreEqual(lineSegmentPoints[i], expectedPoints[i]);
            }
        }
    }
}