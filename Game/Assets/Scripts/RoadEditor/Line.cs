﻿using System.Collections.Generic;
using System.Numerics;
using Vector3 = UnityEngine.Vector3;

public class Line
{
    public Vector3 Start { get; set; }
    public Vector3 End { get; set; }

    public int StepLength { get; }

    private List<LineSegment> _lineSegments;

    public List<LineSegment> LineSegments
    {
        get { return _lineSegments; }
    }

    public Line(Vector3 start, Vector3 end, int stepLength)
    {
        Start = start;
        End = end;

        _lineSegments = new List<LineSegment>();
    }

    public static List<Vector3> GenerateLineSegmentPoints(Vector3 start, Vector3 end, int stepLength = 1)
    {
        var lineSegmentPoints = new List<Vector3>();

        //  TODO: round end down to nearest step length.

        var vec = end - start;
        var roundedLength = (int)vec.magnitude;
        var steps = roundedLength / stepLength;

        for (var i = 0; i <= steps; ++i)
        {
            var covered = stepLength * i;

            var t = (float)covered / steps;

            var p = Vector3.Lerp(start, end, t);

            lineSegmentPoints.Add(p);
        }

        return lineSegmentPoints;
    }

    public static List<Vector3> GenerateLineSegmentPoints(Vector3 start, Vector3 end, Vector3 controlPoint,
        int stepLength = 1)
    {
        var lineSegmentPoints = new List<Vector3>();

        var vec = end - start;
        var roundedLength = (int)vec.magnitude;
        var steps = roundedLength / stepLength;

        for (var i = 0; i <= steps; ++i)
        {
            var covered = stepLength * i;

            var t = (float)covered / steps;

            var p = Vector3.Lerp(Vector3.Lerp(start, controlPoint, t), Vector3.Lerp(controlPoint, end, t), t);

            lineSegmentPoints.Add(p);
        }

        return lineSegmentPoints;
    }
}