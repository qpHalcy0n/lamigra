﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vector3 = UnityEngine.Vector3;

public enum State
{
    Idle,
    PlacedStartPoint,
    PlacedEndPoint,
};

public class RoadEditor : MonoBehaviour
{
    private List<Line> Lines { get; set; }
    private Line CurrentLine { get; set; }

    private bool ClickStarted { get; set; }

    private List<Vector3> Path { get; set; }

    private GameObject RoadObject { get; set; }

    private List<GameObject> RoadSegments { get; set; }

    private State State { get; set; } = State.Idle;

    private Vector3 StartPoint { get; set; }
    private Vector3 EndPoint { get; set; }
    private Vector3 ControlPoint { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        Lines = new List<Line>();
        Path = new List<Vector3>();
        RoadSegments = new List<GameObject>();
        
        RoadObject = GameObject.Find("road");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100))
            {
                switch (State)
                {
                    case State.Idle:
                        StartPoint = hit.point;
                        State = State.PlacedStartPoint;
                        break;
                    case State.PlacedStartPoint:
                        EndPoint = hit.point;
                        State = State.PlacedEndPoint;
                        break;
                    case State.PlacedEndPoint:
                        ControlPoint = hit.point;
                        var path = Line.GenerateLineSegmentPoints(StartPoint, EndPoint, ControlPoint, 1);

                        for (int i = 0; i < path.Count-1; ++i)
                        {
                            var rs = Instantiate(RoadObject);

                            rs.transform.position = path[i];
                            rs.transform.LookAt(path[i + 1]);

                            RoadSegments.Add(rs);
                        }

                        Path.AddRange(path);

                        State = State.Idle;
                        break;
                }
            }
        }

        if (State == State.PlacedStartPoint)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100))
            {
                Debug.DrawLine(StartPoint, hit.point, Color.red);
            }
        }

        if (State == State.PlacedEndPoint)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100))
            {
                Debug.DrawLine(StartPoint, EndPoint, Color.red);
            }
        }

        for (int i = 0; i < Path.Count-1; ++i)
        {
            Debug.DrawLine(Path[i], Path[i + 1], Color.red);
        }


        //if (Input.GetMouseButton(0))
        //{
        //    if (ClickStarted)
        //    {
        //        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100))
        //        {
        //            if (CurrentLine != null)
        //            {
        //                CurrentLine.End = hit.point;
        //                //  CurrentLine.Road.transform.LookAt(hit.point);

        //                /*
        //                var length = (CurrentLine.End.Value - CurrentLine.Start.Value).magnitude;

        //                var mesh = RoadObject.GetComponent<MeshFilter>().mesh;

        //                Debug.Log($"{length}");

        //                if (length > mesh.bounds.size.x)
        //                {
        //                    var segments = (int)(length / mesh.bounds.size.x);

        //                    Debug.Log($"{segments.ToString()}");
        //                }*/
        //            }
        //        }
        //    }
        //}

        //if (Input.GetMouseButtonDown(0))
        //{
        //    if (!ClickStarted)
        //    {
        //        Debug.Log("Click Started");
        //        ClickStarted = true;

        //        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100))
        //        {
        //            //CurrentLine = new Line {Start = hit.point};
        //            //CurrentLine.Road.transform.position = hit.point;
        //        }
        //    }
        //}

        //if (Input.GetMouseButtonUp(0))
        //{
        //    if (ClickStarted)
        //    {
        //        Debug.Log("Click Ended");

        //        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 100))
        //        {
        //            if (CurrentLine != null)
        //            {
        //                CurrentLine.End = hit.point;
        //                Lines.Add(CurrentLine);
        //            }
        //        }

        //        ClickStarted = false;
        //    }
        //}

        //if (CurrentLine?.Start != null && CurrentLine?.End != null)
        //{
        //    //Debug.DrawLine(CurrentLine.Start.Value, CurrentLine.End.Value, Color.red);
        //}

        //foreach (var line in Lines)
        //{
        //    /*
        //    if (line.Start.HasValue && line.End.HasValue)
        //    {
        //        //var length = (line.End.Value - line.Start.Value).magnitude;

        //        //var mesh = RoadObject.GetComponent<MeshFilter>().mesh;

        //        //if (length > mesh.bounds.size.x)
        //        //{
        //        //    var segments = (int)length % (int)mesh.bounds.size.x;

        //        //    Debug.Log($"{segments.ToString()}");
        //        //}
        //    }*/
        //}
    }
}
