﻿using System.Collections.Generic;
using UnityEngine;

public static class VectorExtensions
{
    public static Vector2 SetX(this Vector2 aVec, float aXValue)
    {
        aVec.x = aXValue;
        return aVec;
    }

    public static Vector2 SetY(this Vector2 aVec, float aYValue)
    {
        aVec.y = aYValue;
        return aVec;
    }

    public static Vector2 GetClosest(this Vector2 aPosition, IEnumerable<Vector2> aPoints)
    {
        Vector2 closest = Vector2.zero;
        float shortestDistance = Mathf.Infinity;

        foreach (Vector2 point in aPoints)
        {
            float distance = (aPosition - point).sqrMagnitude;

            if (!(distance < shortestDistance)) continue;

            closest = point;
            shortestDistance = distance;
        }

        return closest;
    }

    public static Vector3 SetX(this Vector3 aVec, float aXValue)
    {
        aVec.x = aXValue;
        return aVec;
    }

    public static Vector3 SetY(this Vector3 aVec, float aYValue)
    {
        aVec.y = aYValue;
        return aVec;
    }

    public static Vector3 SetZ(this Vector3 aVec, float aZValue)
    {
        aVec.z = aZValue;
        return aVec;
    }

    public static Vector3 GetClosest(this Vector3 aPosition, IEnumerable<Vector3> aPoints)
    {
        Vector3 closest = Vector3.zero;
        float shortestDistance = Mathf.Infinity;

        foreach (Vector3 point in aPoints)
        {
            float distance = (aPosition - point).sqrMagnitude;

            if (!(distance < shortestDistance)) continue;

            closest = point;
            shortestDistance = distance;
        }

        return closest;
    }

    public static Vector4 SetX(this Vector4 aVec, float aXValue)
    {
        aVec.x = aXValue;
        return aVec;
    }

    public static Vector4 SetY(this Vector4 aVec, float aYValue)
    {
        aVec.y = aYValue;
        return aVec;
    }

    public static Vector4 SetZ(this Vector4 aVec, float aZValue)
    {
        aVec.z = aZValue;
        return aVec;
    }

    public static Vector4 SetW(this Vector4 aVec, float aWValue)
    {
        aVec.w = aWValue;
        return aVec;
    }
}
