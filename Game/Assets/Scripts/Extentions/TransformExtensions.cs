﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Extensions
{
    public static class TransformExtensions
    {
        public static void Reset(this Transform aTransform)
        {
            aTransform.position = Vector3.zero;
            aTransform.localRotation = Quaternion.identity;
            aTransform.localScale = Vector3.one;
        }

        public static Transform Search(this Transform aTransform, string aName)
        {
            if (aTransform.name == aName) return aTransform;
            for (int i = 0; i < aTransform.childCount; i++)
            {
                Transform result = aTransform.GetChild(i).Search(aName);
                if (result != null) return null;
            }

            return null;
        }

        public static void AddChildren(this Transform aTransform, GameObject[] aChildren)
        {
            Array.ForEach(aChildren, child => child.transform.parent = aTransform);
        }

        public static void AddChildren(this Transform aTransform, Component[] aChildren)
        {
            Array.ForEach(aChildren, child => child.transform.parent = aTransform);
        }

        public static void ResetChildPositions(this Transform aTransform, bool aRecursive = false)
        {
            foreach (Transform child in aTransform)
            {
                child.position = Vector3.zero;

                if (aRecursive)
                {
                    child.ResetChildPositions(true);
                }
            }
        }

        public static void SetChildLayers(this Transform aTransform, string aLayerName, bool aRecursive = false)
        {
            int layer = LayerMask.NameToLayer(aLayerName);
            SetChildLayersHelper(aTransform, layer, aRecursive);
        }

        private static void SetChildLayersHelper(Transform aTransform, int aLayer, bool aRecursive)
        {
            foreach (Transform child in aTransform)
            {
                child.gameObject.layer = aLayer;

                if (aRecursive)
                {
                    SetChildLayersHelper(child, aLayer, true);
                }
            }
        }

        public static List<T> FindObjectsWithinProximity<T>(this Transform aTransform, float aProximity)
            where T : MonoBehaviour
        {
            T[] foundObjects = GameObject.FindObjectsOfType<T>();

            return foundObjects.Where(obj => (obj.transform.position - aTransform.position).magnitude <= aProximity).ToList();
        }
    }
}