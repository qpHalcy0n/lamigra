﻿using UnityEngine;

namespace Core.Extensions
{
    public static class GameObjectExtensions
    {
        public static T GetOrAddComponent<T>(this GameObject aGameObject) where T : Component
        {
            return aGameObject.GetComponent<T>() ?? aGameObject.AddComponent<T>();
        }

        public static bool HasComponent<T>(this GameObject aGameObject) where T : Component
        {
            return aGameObject.GetComponent<T>() != null;
        }

        public static void SetLayerRecursively(this GameObject aGameObject, int aLayer)
        {
            aGameObject.layer = aLayer;
            foreach (Transform child in aGameObject.transform)
            {
                child.gameObject.SetLayerRecursively(aLayer);
            }
        }

        public static void MoveChildren(this GameObject aFrom, GameObject aTo)
        {
            Transform[] children = new Transform[aFrom.transform.childCount];

            for (int i = 0; i < aFrom.transform.childCount; i++)
            {
                children[i] = aFrom.transform.GetChild(i);
            }

            foreach (var child in children)
            {
                child.SetParent(aTo.transform);
            }
        }

        public static GameObject FindChild(this GameObject aGameObject, string aName)
        {
            for (int i = 0; i < aGameObject.transform.childCount; i++)
            {
                Transform child = aGameObject.transform.GetChild(i);
                GameObject foundGameObject = child.gameObject.FindChild(aName);

                if (foundGameObject != null) return foundGameObject;
                if (aGameObject.transform.GetChild(i).name == aName)
                    return aGameObject.transform.GetChild(i).gameObject;
            }

            return null;
        }

        public static GameObject FindChildAbsolute(this GameObject aGameObject, string[] aNameList)
        {
            return aNameList.Length == 0 ? aGameObject : aGameObject.FindChildAbsolute(aNameList, 0);
        }

        private static GameObject FindChildAbsolute(this GameObject aGameObject, string[] aNameList, int aIndex)
        {
            if (aIndex == aNameList.Length) return aGameObject;

            for (int i = aGameObject.transform.childCount - 1; i >= 0; --i)
            {
                var child = aGameObject.transform.GetChild(i);
                if (child.name == aNameList[aIndex])
                {
                    return child.gameObject.FindChildAbsolute(aNameList, aIndex + 1);
                }
            }

            return null;
        }

        public static Component CopyComponent(this GameObject aDestination, Component aOriginal)
        {
            System.Type type = aOriginal.GetType();
            Component copy = aDestination.AddComponent(type);
            System.Reflection.FieldInfo[] fields =
                type.GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance |
                               System.Reflection.BindingFlags.Public);

            foreach (System.Reflection.FieldInfo field in fields)
            {
                if (field.IsDefined(typeof(SerializeField), false))
                {
                    field.SetValue(copy, field.GetValue(aOriginal));
                }
            }

            return copy;
        }
    }
}