﻿using System.Collections.Generic;
using UnityEngine;

public static class CommonExtensions
{
    public static bool IsLayerInMask(this LayerMask aMask, int aLayer)
    {
        return aMask.value == (aMask.value | 1 << aLayer);
    }

    public static List<T> Shuffle<T>(this List<T> aList)
    {
        List<T> newList = new List<T>();

        while (aList.Count > 0)
        {
            T item = aList[Random.Range(0, aList.Count)];
            aList.Remove(item);

            newList.Add(item);
        }

        return newList;
    }

    public static void Shuffle<T>(this T[] aArray)
    {
        System.Random random = new System.Random();

        int n = aArray.Length;
        for (int i = 0; i < n; i++)
        {
            int r = i + (int) (random.NextDouble() * (n - i));
            T tempObject = aArray[r];
            aArray[r] = aArray[i];
            aArray[i] = tempObject;
        }
    }

    public static bool IsVisibleFrom(this Renderer aRenderer, Camera aCamera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(aCamera);
        return GeometryUtility.TestPlanesAABB(planes, aRenderer.bounds);
    }
}
