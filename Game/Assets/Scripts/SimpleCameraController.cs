﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[AddComponentMenu("Camera-Control/Smooth Mouse Look")]
public class SimpleCameraController : MonoBehaviour
{

	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 30F;
	public float sensitivityY = 30F;
	public float minimumX = -360F;
	public float maximumX = 360F;
	public float minimumY = -60F;
	public float maximumY = 60F;
	public bool camSuspend = true;
	public float wasdSens = 2.0f;

	float rotationX = 0F;
	float rotationY = 0F;
	private List<float> rotArrayX = new List<float>();
	float rotAverageX = 0F;
	private List<float> rotArrayY = new List<float>();
	float rotAverageY = 0F;
	public float frameCounter = 20;
	Quaternion originalRotation;

	private Rigidbody mRigidBody;

//	void OnApplicationFocus(bool hasFocus)
//	{
//		if(!hasFocus)
//		{
//			camSuspend = true;
//			Cursor.lockState = CursorLockMode.None;
//		}
//	}

	void Update()
	{
		mRigidBody.AddForce(Vector3.zero);
		mRigidBody.velocity = Vector3.zero;

		float dt = Time.deltaTime;

		if (Input.GetKeyDown(KeyCode.Tab))
		{
			camSuspend = !camSuspend;
			Cursor.lockState = (camSuspend) ? CursorLockMode.None : CursorLockMode.Locked;
		}

		if (Input.GetKey(KeyCode.W))
		{
//			transform.position += transform.forward.normalized * wasdSens * dt;
			mRigidBody.AddForce(transform.forward.normalized * 1700000.0f * wasdSens);
		}

		if (Input.GetKey(KeyCode.S))
		{
//			transform.position += transform.forward.normalized * -wasdSens * dt;
			mRigidBody.AddForce(transform.forward.normalized * -1700000.0f * wasdSens);
		}

		if (Input.GetKey(KeyCode.A))
		{
//			transform.position += transform.right.normalized * -wasdSens * dt;
			mRigidBody.AddForce(transform.right.normalized * -1700000.0f * wasdSens);
		}

		if (Input.GetKey(KeyCode.D))
		{
//			transform.position += transform.right.normalized * wasdSens * dt;
			mRigidBody.AddForce(transform.right.normalized * 1700000.0f * wasdSens);
		}

		if (camSuspend)
			return;

		if (axes == RotationAxes.MouseXAndY)
		{
			//Resets the average rotation
			rotAverageY = 0f;
			rotAverageX = 0f;

			//Gets rotational input from the mouse
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY * dt;
			rotationX += Input.GetAxis("Mouse X") * sensitivityX * dt;

			//Adds the rotation values to their relative array
			rotArrayY.Add(rotationY);
			rotArrayX.Add(rotationX);

			//If the arrays length is bigger or equal to the value of frameCounter remove the first value in the array
			if (rotArrayY.Count >= frameCounter)
			{
				rotArrayY.RemoveAt(0);
			}
			if (rotArrayX.Count >= frameCounter)
			{
				rotArrayX.RemoveAt(0);
			}

			//Adding up all the rotational input values from each array
			for (int j = 0; j < rotArrayY.Count; j++)
			{
				rotAverageY += rotArrayY[j];
			}
			for (int i = 0; i < rotArrayX.Count; i++)
			{
				rotAverageX += rotArrayX[i];
			}

			//Standard maths to find the average
			rotAverageY /= rotArrayY.Count;
			rotAverageX /= rotArrayX.Count;

			//Clamp the rotation average to be within a specific value range
			rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);
			rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);

			//Get the rotation you will be at next as a Quaternion
			//            Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
			//            Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);
			Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, Vector3.left);
			Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);

			//Rotate
			transform.localRotation = originalRotation * xQuaternion * yQuaternion;
		}
		else if (axes == RotationAxes.MouseX)
		{
			rotAverageX = 0f;
			rotationX += Input.GetAxis("Mouse X") * sensitivityX;
			rotArrayX.Add(rotationX);
			if (rotArrayX.Count >= frameCounter)
			{
				rotArrayX.RemoveAt(0);
			}
			for (int i = 0; i < rotArrayX.Count; i++)
			{
				rotAverageX += rotArrayX[i];
			}
			rotAverageX /= rotArrayX.Count;
			rotAverageX = ClampAngle(rotAverageX, minimumX, maximumX);
			Quaternion xQuaternion = Quaternion.AngleAxis(rotAverageX, Vector3.up);
			transform.localRotation = originalRotation * xQuaternion;
		}
		else
		{
			rotAverageY = 0f;
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotArrayY.Add(rotationY);
			if (rotArrayY.Count >= frameCounter)
			{
				rotArrayY.RemoveAt(0);
			}
			for (int j = 0; j < rotArrayY.Count; j++)
			{
				rotAverageY += rotArrayY[j];
			}
			rotAverageY /= rotArrayY.Count;
			rotAverageY = ClampAngle(rotAverageY, minimumY, maximumY);
			Quaternion yQuaternion = Quaternion.AngleAxis(rotAverageY, Vector3.left);
			transform.localRotation = originalRotation * yQuaternion;
		}
	}
	void Start()
	{
		Cursor.lockState = CursorLockMode.None;
		mRigidBody = GetComponent<Rigidbody>();
		if (mRigidBody)
			mRigidBody.freezeRotation = true;
		originalRotation = transform.localRotation;
	}
	public static float ClampAngle(float angle, float min, float max)
	{
		angle = angle % 360;
		if ((angle >= -360F) && (angle <= 360F))
		{
			if (angle < -360F)
			{
				angle += 360F;
			}
			if (angle > 360F)
			{
				angle -= 360F;
			}
		}
		return Mathf.Clamp(angle, min, max);
	}
}