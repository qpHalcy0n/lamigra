﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

#region Data Structures
internal struct BuildingData
{
    internal bool active;
}
#endregion

internal class BuildingAddEvent : UnityEvent<GameObject, BuildingData> { }
internal class BuildingRemovedEvent : UnityEvent<GameObject> { }

internal class BuildingManager : MonoBehaviour
{
    #region Static Accessor
    internal static BuildingAddEvent AddBuilding;
    internal static BuildingRemovedEvent RemoveBuilding;
    #endregion

    #region Private Data
    private static readonly Dictionary<GameObject, BuildingData> mBuildings = new Dictionary<GameObject, BuildingData>();
    #endregion

    #region Internal Functions
    internal void Awake()
    {
        AddBuilding = new BuildingAddEvent();
        AddBuilding.AddListener(RegisterBuilding);

        RemoveBuilding = new BuildingRemovedEvent();
        RemoveBuilding.AddListener(DeregisterBuilding);
    }

    internal static List<GameObject> GetBuildings(bool aIsActive)
    {
        List<GameObject> objects = new List<GameObject>();

        foreach (var obj in mBuildings)
        {
            if (!aIsActive)
            {
                objects.Add(obj.Key);
            }

            if (aIsActive && obj.Value.active)
            {
                objects.Add(obj.Key);
            }
        }

        return objects;
    }

    internal static BuildingData GetBuildingData(GameObject aObject)
    {
        return mBuildings[aObject];
    }

    internal void RegisterBuilding(GameObject aObject, BuildingData aData)
    {
        mBuildings.Add(aObject, aData);
    }

    internal void DeregisterBuilding(GameObject aObject)
    {
        mBuildings.Remove(aObject);
    }
    #endregion
}
