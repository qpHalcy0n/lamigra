﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunDirectionScript : MonoBehaviour
{
    [Range(-90.0f, 90.0f)]
    public float    latitude = 52.0907f;

    [Range(-180.0f, 180.0f)]
    public float    longitude = 5.1214f;

    [Range(0, 23)]
    public int    hour = 20;

    [Range(0, 59)]
    public int    minute = 51;

    [Range(0, 365)]
    public int    day = 229;

    [Range(-12, 12)]
    public int      utcDeviation = 2;

    public bool     useSystemTime = false;


    private float              x, y, z;
    private DateTime           mCurrentDateTime;
    public float               mDeclinationAngle;
    private float              mSunriseHourAngle;
    public float               mHourAngle;
    public float               mSunElevation;
    public float               mSunAzimuth;
    private float              mSunriseHour;
    private float              mHour;

    private const float           PiDiv180 = Mathf.PI / 180.0f;
    private const float           TwoPi     = Mathf.PI * 2.0f;

	public Vector3 GetLightVector()
	{
		return new Vector3(x, y, z).normalized;
	}

    private void GetCurrentTime(out int aDay, out int aHour, out int aMinute)
    {
        if(useSystemTime)
        {
            aDay = mCurrentDateTime.DayOfYear;
            aHour = mCurrentDateTime.Hour;
            aMinute = mCurrentDateTime.Minute;
        }

        else
        {
            aDay = day;
            aHour = hour;
            aMinute = minute;    
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        mCurrentDateTime = DateTime.Now;
    }

    // Update is called once per frame
    void Update()
    {
        // http://mypages.iit.edu/~maslanka/SolarGeo.pdf
        mCurrentDateTime = DateTime.Now;
        GetCurrentTime(out day, out hour, out minute);
        float minuteFrac = (float)minute / 60.0f;
        mHour = (float)hour + minuteFrac;

        mDeclinationAngle = Mathf.Rad2Deg * Mathf.Asin(0.39795f * Mathf.Cos(Mathf.Deg2Rad * (0.98563f * (day - 173.0f))));

        // https://www.pveducation.org/pvcdrom/properties-of-sunlight/solar-time
        // Correct local to solar time
        float lstm = 15.0f * utcDeviation;
        float B = (360.0f / 365.0f) * (day - 81);
        float eot = 9.87f * Mathf.Sin(2.0f * B) - 7.53f * Mathf.Cos(B) - 1.5f * Mathf.Sin(B);
        float tc = 4.0f * (longitude - lstm) + eot;
        float lst = mHour + (tc / 60.0f);

        mHourAngle = 15.0f * (lst - 12.0f);
        mSunElevation = Mathf.Asin( (Mathf.Sin(Mathf.Deg2Rad * mDeclinationAngle) * Mathf.Sin(Mathf.Deg2Rad * latitude)) + 
                                    (Mathf.Cos(Mathf.Deg2Rad * mDeclinationAngle) * Mathf.Cos(Mathf.Deg2Rad * mHourAngle) * Mathf.Cos(Mathf.Deg2Rad * latitude)));

        mSunElevation *= Mathf.Rad2Deg;

        float acosArg = ((Mathf.Sin(Mathf.Deg2Rad * mDeclinationAngle) * Mathf.Cos(Mathf.Deg2Rad * latitude)) - (Mathf.Cos(Mathf.Deg2Rad * mDeclinationAngle) * Mathf.Cos(Mathf.Deg2Rad * mHourAngle) * Mathf.Sin(Mathf.Deg2Rad * latitude))) / 
                          Mathf.Cos(Mathf.Deg2Rad * mSunElevation);

        acosArg = Mathf.Clamp(acosArg, -0.999999f, 0.999999f);
        float A = Mathf.Acos(acosArg);

        A *= Mathf.Rad2Deg;

        if(mHourAngle <= 0)
            mSunAzimuth = A;
        else    
            mSunAzimuth = 360.0f - A;

        x = Mathf.Sin(Mathf.Deg2Rad * mSunAzimuth);
        z = Mathf.Cos(Mathf.Deg2Rad * mSunAzimuth);
        y = Mathf.Sin(Mathf.Deg2Rad * mSunElevation);

        Vector3 sunVec = new Vector3(x, y, z);
        sunVec.Normalize();
        transform.forward = sunVec * -1.0f;
    }
}