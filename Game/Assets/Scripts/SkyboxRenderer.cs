﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxRenderer : MonoBehaviour
{ 
	public RenderTexture SkyboxLeft;
	public RenderTexture SkyboxRight;
	public RenderTexture SkyboxUp;
	public RenderTexture SkyboxDown;
	public RenderTexture SkyboxFront;
	public RenderTexture SkyboxBack;

	private SkyLightingScript	mSkyLightingScript;
	private Material			mMaterial;

	// Left, Right, Front, Back, Up, Down
	private Vector3[]			mViewVectors = { new Vector3(1.0f, 0.0f, 0.0f),		
												 new Vector3(-1.0f, 0.0f, 0.0f),
												 new Vector3(0.0f, 0.0f, 1.0f),
												 new Vector3(0.0f, 0.0f, -1.0f),
												 new Vector3(0.0f, 1.0f, 0.0f),
												 new Vector3(0.0f, -1.0f, 0.0f)
											   };

	private GameObject[] mDummyGameObjects = new GameObject[6];
	private Camera[] mDummyCams = new Camera[6];

    // Start is called before the first frame update
    void Start()
    {
		mSkyLightingScript = transform.parent.GetComponentInChildren<SkyLightingScript>();
		mMaterial = new Material(Shader.Find("Quadrion/Skybox"));

		for(int i = 0; i < 6; i++)
		{
			mDummyGameObjects[i] = new GameObject();
			Camera thisCam = mDummyGameObjects[i].AddComponent<Camera>();
			thisCam.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
			thisCam.transform.forward = mViewVectors[i];
			thisCam.cullingMask = 0;
			thisCam.depth = -20;
			thisCam.fieldOfView = 90.0f;
			thisCam.aspect = 1.0f;
			thisCam.nearClipPlane = 1.0f;
			thisCam.farClipPlane = 10000.0f;
			mDummyCams[i] = thisCam;
//			mDummyGa[i].transform.position = new Vector3(0.0f, 0.0f, 0.0f);
//			mDummyGa[i].transform.forward = mViewVectors[i];
		}
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	private RenderTexture GetRenderTextureForPass(int pass)
	{
		if(pass == 0)
			return SkyboxLeft;
		else if(pass == 1)
			return SkyboxRight;
		else if(pass == 2)
			return SkyboxFront;
		else if(pass == 3)
			return SkyboxBack;
		else if(pass == 4)
			return SkyboxUp;
		else
			return SkyboxDown;
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Vector3 L = mSkyLightingScript.GetLightVector();

//		Vector3 V = Camera.main.transform.forward;
//		Vector3 V = mDummyCams[0].transform.forward;
		Vector3 V = new Vector3(0.0f, 0.0f, 0.0f);
		Matrix4x4 inverseV = Camera.main.worldToCameraMatrix.inverse;
		Matrix4x4 inverseP = Camera.main.projectionMatrix.inverse;

		// Constant params 
		mMaterial.SetVector("_L", L);
		mMaterial.SetFloat("_rayleighBrightness", mSkyLightingScript.rayleighBrightness);
		mMaterial.SetFloat("_rayleighCollectionPower", mSkyLightingScript.rayleighCollectionPower);
		mMaterial.SetFloat("_rayleighStrength", mSkyLightingScript.rayleighStrength);
		mMaterial.SetFloat("_mieDistribution", mSkyLightingScript.mieDistribution);
		mMaterial.SetFloat("_mieCollectionPower", mSkyLightingScript.mieCollectionPower);
		mMaterial.SetFloat("_mieStrength", mSkyLightingScript.mieStrength);
		mMaterial.SetFloat("_mieBrightness", mSkyLightingScript.mieBrightness);
		mMaterial.SetFloat("_scatterStrength", mSkyLightingScript.scatterStrength);
		mMaterial.SetFloat("_surfaceHeight", mSkyLightingScript.surfaceHeight);

		// Per camera params
		for(int i = 0; i < 6; i++)
		{
			inverseV = mDummyCams[i].worldToCameraMatrix.inverse;
			inverseP = mDummyCams[i].projectionMatrix.inverse;
			V = mDummyCams[i].transform.forward;

			mMaterial.SetVector("_V", V);
			mMaterial.SetMatrix("_inverseV", inverseV);
			mMaterial.SetMatrix("_inverseP", inverseP);
			mMaterial.SetInt("_faceID", i);

			Graphics.Blit(source, GetRenderTextureForPass(i), mMaterial);
		}
	}
}
