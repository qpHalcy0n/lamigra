﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionCamScript : MonoBehaviour
{
//    public float waterHeight = 200.0f;
	public float ang;
	public float mainCamAngle;
	public float reflCamAngle;

    private Camera      mCameraComponent;
	private Matrix4x4	mViewMatrix;
	private Matrix4x4	mProjectionMatrix;

	private float		waterHeight;

	public void SetWaterElevation(float aWaterElevation)
	{
		waterHeight = aWaterElevation;	
	}

	public Matrix4x4 GetReflectionViewMatrix()
	{
		return mViewMatrix;
	}

	public Matrix4x4 GetReflectionProjectionMatrix()
	{
		return mProjectionMatrix;
	}

    // Start is called before the first frame update
    void Start()
    {
        mCameraComponent = GetComponent<Camera>();
	}

    void OnPreCull()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 camPos = Camera.main.transform.position;
        Vector3 viewVec = Camera.main.transform.forward.normalized;

        float heightAboveWater = camPos.y - waterHeight;
        Vector3 reflectedCamPos = new Vector3(camPos.x, waterHeight - heightAboveWater, camPos.z);

		
		transform.rotation = Camera.main.transform.rotation;
		transform.position = Camera.main.transform.position;
//		transform.position = reflectedCamPos;
		Vector3 horizontal = new Vector3(viewVec.x, 0.0f, viewVec.z);
		Vector3 forwardProj = Camera.main.transform.forward.normalized;
//		ang = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(horizontal, forwardProj));
		ang = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(forwardProj, Vector3.down));
		ang = ang - 90.0f;
		mainCamAngle = ang;


		transform.Rotate(Vector3.right, 2.0f * ang, Space.Self);
		transform.Rotate(Vector3.forward, 180.0f, Space.Self);
		transform.position = reflectedCamPos;
		

		mCameraComponent.fieldOfView = Camera.main.fieldOfView;
        mCameraComponent.aspect = Camera.main.aspect;
        mCameraComponent.nearClipPlane = Camera.main.nearClipPlane;
        mCameraComponent.farClipPlane = Camera.main.farClipPlane;

		mViewMatrix = mCameraComponent.worldToCameraMatrix;
		mProjectionMatrix = mCameraComponent.projectionMatrix;

		reflCamAngle = Mathf.Rad2Deg * Mathf.Acos(Vector3.Dot(transform.forward.normalized, Vector3.down));
    }

}
