﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelVisibility : MonoBehaviour
{
    public GameObject Target { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleVisibility()
    {
        if (Target == null)
            return;

        var canvarRenderer = Target.GetComponent<CanvasRenderer>();

        if (canvarRenderer == null)
            return;

        if (canvarRenderer.GetAlpha() >= 0.9f)
        {
            canvarRenderer.SetAlpha(0.1f);
        }
        else
        {
            canvarRenderer.SetAlpha(1.0f);
        }
        
    }
}
