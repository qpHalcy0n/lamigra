﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGui : MonoBehaviour
{
    public GameObject BuildPanel;

    private Animation drawerAnimations;
    private bool isBuildDrawerShowing = true;

    // Start is called before the first frame update
    void Start()
    {
        SetBuildPanelVisibility(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        if (BuildPanel == null)
            return;

        drawerAnimations = BuildPanel.GetComponent<Animation>();
    }

    public void ToggleBuildPanel()
    {
        SetBuildPanelVisibility(!GetBuildPanelVisibility());
    }

    public bool GetBuildPanelVisibility()
    {
        return isBuildDrawerShowing;
    }

    public void SetBuildPanelVisibility(bool visibility)
    {
        if (BuildPanel == null)
            return;

        isBuildDrawerShowing = visibility;
        BuildPanel.SetActive(visibility);

        //if (drawerAnimations != null)
        //{
        //    if (visibility)
        //        drawerAnimations.Play("DrawerClose");
        //    else
        //        drawerAnimations.Play("DrawerOpen");
        //}
    }
}
