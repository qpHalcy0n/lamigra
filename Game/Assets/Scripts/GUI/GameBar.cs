﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;


// NOTE: BE SURE that when you inherit EditorWindow that either this script resides in a folder
// named "Editor" or that it's wrapped in these ifdef blocks. Otherwise the project will not build.
#if UNITY_EDITOR

public class QuickTool : EditorWindow
{
}


#endif