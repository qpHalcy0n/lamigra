﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class FFTTerrainGenerator : MonoBehaviour
{
	[Range(0.0f, 10.0f)]
	public float fogAltitudeCoefC = 1.0f;

	[Range(0.0f, 10.0f)]
	public float fogAltitudeCoefB = 1.0f;

	[Range(0.0f, 1000.0f)]
	public float fogAltitude = 500.0f;

	public float entropy = 2.2f;
	public int nSamplesX = 512;
	public int nSamplesZ = 512;
	public double smoothing = 2.6;
	public float heightScale = 4400.0f;
	public float meshWidth = 5000.0f;
	public float meshDepth = 5000.0f;
	public int nPatchesX = 16;
	public int nPatchesZ = 16;
	public GameObject lightObject;

	private Vector3 terrainMins;
	private Vector3 terrainSpan;
	private float   terrainGreatestLength;

	private float[] heightField;
	private Terrain mTerrain;
	private Material mMaterial;

	private TerrainFog mTerrainFogScript;

	public Terrain GetTerrain()
	{
		return mTerrain;
	}

	private void generateGaussianNoise(float[] dat, int nSamps, float stdDev = 0.1f)
	{
		if (nSamps <= 0)
			return;

		float mean = 0.0f;
		float dev = stdDev;

		System.Random rand = new System.Random();
		for (int i = 0; i < nSamps; ++i)
		{
			float u1 = (float)(1.0 - rand.NextDouble());
			float u2 = (float)(1.0 - rand.NextDouble());
			double rnd = Math.Sqrt(-2.0 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
			float a = mean + dev * (float)rnd;

			dat[i] = a;
		}
	}

	private void fft(ref System.Numerics.Complex[] x)
	{
		int N = x.Length;
		if (N <= 1)
			return;

		// TEST AGAINST LIST 
		System.Numerics.Complex[] even = new System.Numerics.Complex[N / 2];
		System.Numerics.Complex[] odd = new System.Numerics.Complex[N / 2];
		int evenIdx = 0;
		int oddIdx = 0;
		for (int i = 0; i < N; i++)
		{
			if (i % 2 == 0)
				even[evenIdx++] = x[i];
			else
				odd[oddIdx++] = x[i];
		}

		fft(ref even);
		fft(ref odd);

		for (int k = 0; k < N / 2; k++)
		{
			System.Numerics.Complex t = System.Numerics.Complex.FromPolarCoordinates(1.0, -2.0 * Math.PI * k / N) * odd[k];
			x[k] = even[k] + t;
			x[k + N / 2] = even[k] - t;
		}
	}

	private void inverseFFT(ref System.Numerics.Complex[] x)//std::valarray<std::complex<double>> & x)
	{
		for (int i = 0; i < x.Length; i++)
			x[i] = System.Numerics.Complex.Conjugate(x[i]);

		//		x = x.apply(std::conj);
		fft(ref x);
		//		x = x.apply(std::conj);
		for (int i = 0; i < x.Length; i++)
			x[i] = System.Numerics.Complex.Conjugate(x[i]);

		double sz = (double)x.Length;  // static_cast<double>(x.size());
		System.Numerics.Complex t = new System.Numerics.Complex(sz, sz);
		for (int i = 0; i < x.Length; i++)
			x[i] /= t;
	}

	public void Generate()
	{
		mTerrain = gameObject.GetComponent<Terrain>();

		if (!Mathf.IsPowerOfTwo(nSamplesX) || !Mathf.IsPowerOfTwo(nSamplesZ))
			return;

		int size2 = nSamplesX * nSamplesZ;

		heightField = new float[size2];
		generateGaussianNoise(heightField, size2, entropy);

		double min = Double.MaxValue;
		double max = Double.MinValue;
		for (int i = 0; i < size2; ++i)
		{
			if (heightField[i] < min)
				min = heightField[i];
			if (heightField[i] > max)
				max = heightField[i];
		}

		int width = nSamplesX;
		int height = nSamplesZ;
		int nVerts = width * height;
		int nVertsX = width;
		int nVertsZ = height;
		nSamplesX = nVertsX;
		nSamplesZ = nVertsZ;
		int idx = 0;
		int valsIdx = 0;
		float displ = 0.0f;

		System.Numerics.Complex[] vals = new System.Numerics.Complex[nVertsX * nVertsZ];
		System.Numerics.Complex[] tmp = new System.Numerics.Complex[nVertsX];

		int sx = 0;
		int sz = 0;

		for (int z = 0; z < nVertsZ; ++z)          // j 
		{
			for (int x = 0; x < nVertsX; ++x)      // i
			{
				double val = heightField[(z * nVertsX + x)];
				displ = (float)val;

				// Build the height data in Y only for the fft //
				System.Numerics.Complex a = new System.Numerics.Complex(displ, 0.0);
				vals[valsIdx++] = a;

				if (x % 8 == 0)
					sx++;
			}

			sx = 0;
			if (z % 8 == 0)
				sz++;
		}

		// Apply fft horizontally //
		idx = 0;
		for (int i = 0; i < nVertsX; ++i)
		{
			idx = i;

			// Pack the row vector //
			for (int j = 0; j < nVertsZ; ++j)
			{
				//				std::complex<double> a(vals[idx]);
				System.Numerics.Complex a = vals[idx];
				tmp[j] = a;
				idx += nVertsZ;
			}

			// Perform fft on row //
			idx = i;

			// Put values back into grid //
			for (int j = 0; j < nVertsX; ++j)
			{
				vals[idx] = tmp[j];
				idx += nVertsZ;
			}
		}

		// Apply fft vertically //
		idx = 0;
		for (int i = 0; i < nVertsX; ++i)
		{
			idx = i * nVertsZ;
			for (int j = 0; j < nVertsZ; ++j)
			{
				System.Numerics.Complex a = vals[idx];
				tmp[j] = a;
				idx++;
			}

			fft(ref tmp);
			idx = i * nVertsZ;

			for (int j = 0; j < nVertsZ; ++j)
			{
				vals[idx] = tmp[j];
				idx++;
			}
		}

		// Apply Pink Noise or 1/(f^p) filter //
		double f;
		//	float power = static_cast<float>(smoothing);		// 2.0
		double power = smoothing;
		double fN = nVertsZ;
		int iN2, jN2;
		for (int j = 0; j < nVertsX; ++j)
		{
			for (int i = 0; i < nVertsZ; ++i)
			{
				idx = i + j * nVertsZ;

				iN2 = i - nVertsZ / 2;
				jN2 = j - nVertsZ / 2;
				f = Math.Sqrt((iN2) / fN * (iN2) / fN + (jN2) / fN * (jN2) / fN);
				f = f < 1.0 / nVertsZ ? 1.0 / nVertsZ : f;

				System.Numerics.Complex t = (System.Numerics.Complex)(1.0 / Math.Pow(f, power));
				vals[idx] = vals[idx] * t;
			}
		}

		// Apply Ifft vertically //
		idx = 0;
		for (int i = 0; i < nVertsX; ++i)
		{
			idx = i * nVertsX;
			for (int j = 0; j < nVertsZ; ++j)
			{
				//				std::complex<double> a(vals[idx]);
				System.Numerics.Complex a = vals[idx];
				tmp[j] = a;
				idx++;
			}

			inverseFFT(ref tmp);
			idx = i * nVertsX;

			for (int j = 0; j < nVertsZ; ++j)
			{
				vals[idx] = tmp[j];
				idx++;
			}
		}

		// Apply Ifft horizontally //
		// Scale and pack values back into vertex list at the same time //
		idx = 0;
		int insertIdx = 0;
		float maxHeight = float.MinValue;//		DBL_MIN;
		float minHeight = float.MaxValue;//DBL_MAX;
		for (int i = 0; i < nVertsX; ++i)
		{
			idx = i;

			// Pack the row vector //
			for (int j = 0; j < nVertsZ; ++j)
			{
				//				std::complex<double> a(vals[idx]);
				System.Numerics.Complex a = vals[idx];
				tmp[j] = a;
				idx += nVertsZ;
			}

			// Perform fft on row //
			//			qtl::ifft(tmp);
			inverseFFT(ref tmp);
			idx = i;

			// Put values back into grid //
			for (int j = 0; j < nVertsZ; ++j)
			{
				vals[idx] = tmp[j];
				idx += nVertsZ;

				//				std::complex<double> conj;
				//				conj = std::conj(tmp[j]);
				System.Numerics.Complex conj = System.Numerics.Complex.Conjugate(tmp[j]);
				conj = tmp[j] * conj;
				//				double val = std::abs(conj.real());
				float val = Mathf.Abs((float)conj.Real);
				if (val > maxHeight)
					maxHeight = val;
				if (val < minHeight)
					minHeight = val;

				heightField[insertIdx++] = val;
			}
		}

		// TODO: eliminate
		//		h = new uint16_t[width * height];
		//	float* h = new float[width * height];
		float heightRange = maxHeight - minHeight;
		//		minHeight = Double.MaxValue;
		//		maxHeight = Double.MinValue;

//		mTerrainMinHeight = float.MaxValue;
//		mTerrainMaxHeight = float.MinValue;

		for (int i = 0; i < nVerts; ++i)
		{
			int zC = (i / nVertsZ);
			int xC = i - (zC * nVertsX);
			double normalizedHeight = heightField[i] / heightRange; // floor?
																	//			double maxHeight = static_cast<double>(heightScale);
																	//			h[i] = static_cast<uint16_t>(normalizedHeight* 65535.0);
																	//		h[i] = static_cast<float>(normalizedHeight * maxHeight);
			heightField[i] = (float)normalizedHeight;
			//			heightField[i] /= heightRange;
			//			heightField[i] *= heightScale;

			//			if (heightField[i] < mTerrainMinHeight)
			//				mTerrainMinHeight = heightField[i];
			//			if (heightField[i] > mTerrainMaxHeight)
			//				mTerrainMaxHeight = heightField[i];
		}

		float[,] heightMatrix = new float[nSamplesX, nSamplesZ];
		int readIdx = 0;
		for (int j = 0; j < nSamplesZ; j++)
		{
			for (int i = 0; i < nSamplesX; i++)
			{
				heightMatrix[i, j] = heightField[readIdx++];
			}
		}

		mTerrain.terrainData.heightmapResolution = nSamplesX + 1;
        mTerrain.terrainData.SetHeights(0, 0, heightMatrix);
	}

	public Vector3 GetTerrainMins()
	{
		return terrainMins;
	}

	public Vector3 GetTerrainSpan()
	{
		return terrainSpan;
	}

	// Start is called before the first frame update
	void Start()
    {
		Generate();
		mMaterial = mTerrain.materialTemplate;
		
		terrainSpan = mTerrain.terrainData.size;
		terrainMins = mTerrain.gameObject.transform.position;

		terrainGreatestLength = Mathf.Sqrt(terrainSpan.x * terrainSpan.x + terrainSpan.z * terrainSpan.z);
	}


	// Update is called once per frame
	void Update()
    {
		mTerrainFogScript = gameObject.GetComponentInChildren<TerrainFog>();
		Texture2D persistentFogTex = mTerrainFogScript.GetPersistentTexture();
		Texture2D extinctFogTex = mTerrainFogScript.GetExtinctTexture();
		Texture2D roamingFogTex = mTerrainFogScript.GetRoamingTexture();

		Vector4 fogCoefs = new Vector4(fogAltitudeCoefB, fogAltitudeCoefC, fogAltitude, 0.0f);
		Vector3 L = lightObject.transform.forward.normalized;
		mMaterial.SetVector("_CamPosWorld", Camera.main.transform.position);
		mMaterial.SetVector("_L", L);
		mMaterial.SetTexture("_PersistentFogTex", persistentFogTex);
		mMaterial.SetTexture("_ExtinctFogTex", extinctFogTex);
		mMaterial.SetTexture("_RoamingFogTex", roamingFogTex);
		mMaterial.SetFloat("_TerrainSpan", terrainGreatestLength);
		mMaterial.SetVector("_FogAltitudeCoefs", fogCoefs);
	}
}
