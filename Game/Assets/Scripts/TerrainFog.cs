﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

public class TerrainFog : MonoBehaviour
{
	public int						fogTextureWidth		= 256;
	public int						fogTextureHeight	= 256;
	public Texture2D				mRoamingFogTexture;
	public Texture2D				mPersistentFogTexture;
	public Texture2D				mExtinctFogTexture;
	public byte						inactiveColor		= 0x80;
	public byte						activeColor			= 0xFF;
	public bool						useFogOfWar			= true;

	private FFTTerrainGenerator		mTerrainGenerator;
	private Vector3					mTerrainMins;
	private Vector3					mTerrainSpan;
	private float					mPixelWidth;
	private float					mPixelHeight;
//	private byte[]					mFogGrid; 
	private sbyte[]					mFogGridDepth;		// number of units that can see a particular cell
	private byte[]					mPersistentFogGrid;
	private NativeArray<byte>		mRoamingFogGrid;
	private byte[]					mExtinctFogGrid;
	private byte[]					mZeroArray;
	private byte[]					mOneArray;
    
	public Texture2D GetPersistentTexture()
	{
		return mPersistentFogTexture;
	}

	public Texture2D GetExtinctTexture()
	{
		return mExtinctFogTexture;
	}

	public Texture2D GetRoamingTexture()
	{
		return mRoamingFogTexture;
	}

	private void GetCellFromPosition(Vector3 pos, out int cellX, out int cellZ)
	{
		float xPercent = (pos.x - mTerrainMins.x) / mTerrainSpan.x;
		float zPercent = (pos.z - mTerrainMins.z) / mTerrainSpan.z;
		xPercent *= (float)(fogTextureWidth);
		zPercent *= (float)(fogTextureHeight);
		
		cellX = Mathf.FloorToInt(xPercent);
		cellZ = Mathf.FloorToInt(zPercent);
	}

	private Vector3 GetCenterOfCell(int cellX, int cellZ)
	{
		float x = ((mPixelWidth * cellX) + (mPixelWidth / 2.0f)) + mTerrainMins.x;
		float z = ((mPixelHeight * cellZ) + (mPixelHeight / 2.0f)) + mTerrainMins.z;
		float y = 0.0f;
		return new Vector3(x, y, z);
	}

	private int GetNumLinearCellsVisibleX(float visibilityRadius)
	{
		return Mathf.FloorToInt(visibilityRadius / mPixelWidth);
	}

	private int GetNumLinearCellsVisibleZ(float visibilityRadius)
	{
		return Mathf.FloorToInt(visibilityRadius / mPixelHeight);
	}

	private int GetPixelIndex(int cellX, int cellZ)
	{
		return (fogTextureWidth * cellZ) + cellX;
	}

	// set "add" to true to add unit to cell, set to false to remove unit from cell
	private void PopulatePersistentCells(Vector3 pos, float visibilityRadius, bool add)
	{
		int pixX, pixY;
		GetCellFromPosition(pos, out pixX, out pixY);

		int cellsX = GetNumLinearCellsVisibleX(visibilityRadius);
		int cellsZ = GetNumLinearCellsVisibleZ(visibilityRadius);
		int maxXCell, minXCell, maxZCell, minZCell;
		maxXCell = (pixX + cellsX > fogTextureWidth - 1) ? fogTextureWidth - 1 : pixX + cellsX;
		minXCell = (pixX - cellsX < 0) ? 0 : pixX - cellsX;
		maxZCell = (pixY + cellsZ > fogTextureHeight - 1) ? fogTextureHeight - 1 : pixY + cellsZ;
		minZCell = (pixY - cellsZ < 0) ? 0 : pixY - cellsZ;

		Vector2 projectedPos = new Vector2(pos.x, pos.z);
		Vector2 test0 = new Vector2();
		Vector2 test1 = new Vector2();
		Vector2 test2 = new Vector2();
		Vector2 test3 = new Vector2();
		float r2 = visibilityRadius * visibilityRadius;
		for (int i = minXCell; i <= maxXCell; i++)
		{
			for (int j = minZCell; j < maxZCell; j++)
			{
				float minX = (i * mPixelWidth) + mTerrainMins.x;
				float maxX = minX + mPixelWidth;
				float minZ = (j * mPixelHeight) + mTerrainMins.z;
				float maxZ = minZ + mPixelHeight;

				test0.Set(minX - projectedPos.x, minZ - projectedPos.y);
				test1.Set(maxX - projectedPos.x, minZ - projectedPos.y);
				test2.Set(maxX - projectedPos.x, maxZ - projectedPos.y);
				test3.Set(minX - projectedPos.x, maxZ - projectedPos.y);

				if ((test0.sqrMagnitude > r2) && (test1.sqrMagnitude > r2) && (test2.sqrMagnitude > r2) && (test3.sqrMagnitude > r2))
					continue;

				int idx = GetPixelIndex(i, j);

				if(add)
					mFogGridDepth[idx]++;
				else
					mFogGridDepth[idx]--;

				if(mFogGridDepth[idx] < 0)
					mFogGridDepth[idx] = 0;

				// When number of units able to view a cell goes to zero or < 0, then the 
				// cell visibility is inactive but "seen". Otherwise it is actively visible (full vis).
				// This prevents errors due to overlap
				if (mFogGridDepth[idx] <= 0)
				{
					mPersistentFogGrid[idx] = 0;
				}

				else
				{
					mExtinctFogGrid[idx] = inactiveColor;
					mPersistentFogGrid[idx] = activeColor;
				}
			}
		}
	}

	private void PopulateRoamingCells(Vector3 pos, float visibilityRadius)
	{
		int pixX, pixY;
		GetCellFromPosition(pos, out pixX, out pixY);

		int cellsX = GetNumLinearCellsVisibleX(visibilityRadius);
		int cellsZ = GetNumLinearCellsVisibleZ(visibilityRadius);
		int maxXCell, minXCell, maxZCell, minZCell;
		maxXCell = (pixX + cellsX > fogTextureWidth - 1) ? fogTextureWidth - 1 : pixX + cellsX;
		minXCell = (pixX - cellsX < 0) ? 0 : pixX - cellsX;
		maxZCell = (pixY + cellsZ > fogTextureHeight - 1) ? fogTextureHeight - 1 : pixY + cellsZ;
		minZCell = (pixY - cellsZ < 0) ? 0 : pixY - cellsZ;

		Vector2 projectedPos = new Vector2(pos.x, pos.z);
		Vector2 test0 = new Vector2();
		Vector2 test1 = new Vector2();
		Vector2 test2 = new Vector2();
		Vector2 test3 = new Vector2();
		float r2 = visibilityRadius * visibilityRadius;
		for (int i = minXCell; i <= maxXCell; i++)
		{
			for (int j = minZCell; j < maxZCell; j++)
			{
				float minX = (i * mPixelWidth) + mTerrainMins.x;
				float maxX = minX + mPixelWidth;
				float minZ = (j * mPixelHeight) + mTerrainMins.z;
				float maxZ = minZ + mPixelHeight;

				test0.Set(minX - projectedPos.x, minZ - projectedPos.y);
				test1.Set(maxX - projectedPos.x, minZ - projectedPos.y);
				test2.Set(maxX - projectedPos.x, maxZ - projectedPos.y);
				test3.Set(minX - projectedPos.x, maxZ - projectedPos.y);

				if ((test0.sqrMagnitude > r2) && (test1.sqrMagnitude > r2) && (test2.sqrMagnitude > r2) && (test3.sqrMagnitude > r2))
					continue;

				int idx = GetPixelIndex(i, j);

				mRoamingFogGrid[idx] = activeColor;
				mExtinctFogGrid[idx] = inactiveColor;
			}
		}
	}

	void Start()
    {
		mTerrainGenerator = gameObject.GetComponentInParent<FFTTerrainGenerator>();

		mTerrainMins = mTerrainGenerator.GetTerrainMins();
		mTerrainSpan = mTerrainGenerator.GetTerrainSpan();

		mRoamingFogTexture = new Texture2D(fogTextureWidth, fogTextureHeight, TextureFormat.R8, false);
		mPersistentFogTexture = new Texture2D(fogTextureWidth, fogTextureHeight, TextureFormat.R8, false);
		mExtinctFogTexture = new Texture2D(fogTextureWidth, fogTextureHeight, TextureFormat.R8, false);

		mPixelWidth = mTerrainSpan.x / (float)(fogTextureWidth);
		mPixelHeight = mTerrainSpan.z / (float)(fogTextureHeight);
//		mFogGrid = new byte[fogTextureWidth * fogTextureHeight];
		mFogGridDepth = new sbyte[fogTextureWidth * fogTextureHeight];
		mPersistentFogGrid = new byte[fogTextureWidth * fogTextureHeight];
		mExtinctFogGrid = new byte[fogTextureWidth * fogTextureHeight];
		mZeroArray = new byte[fogTextureWidth * fogTextureHeight];
		mOneArray = new byte[fogTextureWidth * fogTextureHeight];
		for (int i = 0; i < fogTextureWidth * fogTextureHeight; i++)
		{
			mFogGridDepth[i] = 0;
			mPersistentFogGrid[i] = 0;
			mExtinctFogGrid[i] = 0;
			mZeroArray[i] = 0;
			mOneArray[i] = 0xFF;
		}
		mRoamingFogGrid = new NativeArray<byte>(mZeroArray, Unity.Collections.Allocator.Persistent);

		BuildingManager.AddBuilding.AddListener(BuildingAdded);
		BuildingManager.RemoveBuilding.AddListener(BuildingRemoved);
		UnitManager.ChangeUnit.AddListener(UnitMoved);
	}

    // Update is called once per frame
    void Update()
    {
		List<GameObject> units = UnitManager.GetUnits(true, true);
		foreach(var unit in units)
		{
			PopulateRoamingCells(unit.transform.position, 1000.0f);	
		}

		if(useFogOfWar)
		{
			var persistentData = mPersistentFogTexture.GetRawTextureData<byte>();
			persistentData.CopyFrom(mPersistentFogGrid);
			mPersistentFogTexture.Apply();

			var extinctData = mExtinctFogTexture.GetRawTextureData<byte>();
			extinctData.CopyFrom(mExtinctFogGrid);
			mExtinctFogTexture.Apply();

			mRoamingFogGrid.CopyFrom(mZeroArray);
		}
		
		else
			mRoamingFogGrid.CopyFrom(mOneArray);

		var texData = mRoamingFogTexture.GetRawTextureData<byte>();
			texData.CopyFrom(mRoamingFogGrid);
			mRoamingFogTexture.Apply();

    }

	internal void BuildingAdded(GameObject aObject, BuildingData aData)
	{
		PopulatePersistentCells(aObject.transform.position, 1000.0f, true);
	}

	internal void BuildingRemoved(GameObject aObject)
	{
		PopulatePersistentCells(aObject.transform.position, 1000.0f, false);
	}

	internal void UnitMoved(GameObject aObject, UnitData aData)
	{
		
	}

	public void OnDestroy()
	{
		mRoamingFogGrid.Dispose();
	}
}
