﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TerrainPlacer : MonoBehaviour
{
	public GameObject				mTestPrefab;
	public Material					mPlacingMaterialValid;
	public Material					mPlacingMaterialInvalid;
	public Material					mPlacedMaterial;
	public bool						mFlattenOnPlacement = true;

	public bool						mBuildingPlacementMode = false;

	private Terrain					mTerrain;
	private TerrainGridRenderer		mTerrainGridScript;
	private FFTTerrainGenerator		mTerrainGenerator;
	private TerrainFog				mTerrainFogScript;
	private GameObject				mInstantiatedBldg;
	private GameObject				mCurrentBuilding;
	private MeshRenderer			mCurrentBuildingMeshRenderer;
	private List<GameObject>		mTestObjects;

	public List<GameObject> GetGameObjects()
	{
		return mTestObjects;
	}

	public GameObject GetCurrentObject()
	{
		return mCurrentBuilding;
	}

    // Start is called before the first frame update
    void Start()
    {
		mTerrainGridScript = gameObject.GetComponentInParent<TerrainGridRenderer>();
		mTerrainGenerator = gameObject.GetComponentInParent<FFTTerrainGenerator>();
		mTerrainFogScript = gameObject.GetComponentInParent<TerrainFog>();
		mTerrain = Terrain.activeTerrain;

		mTestObjects = new List<GameObject>();
		mCurrentBuilding = Instantiate(mTestPrefab, new Vector3(), Quaternion.identity);
		mCurrentBuilding.layer = LayerMask.NameToLayer("Default");
		mCurrentBuilding.GetComponent<BoxCollider>().enabled = false;
		mCurrentBuildingMeshRenderer = mCurrentBuilding.GetComponent<MeshRenderer>();
		UnitData rodSucksDicks = new UnitData();
		rodSucksDicks.active = true;
		rodSucksDicks.hasChanged = true;
		UnitManager.AddUnit.Invoke(mCurrentBuilding, rodSucksDicks);
    }

	void Update()
	{
		Destroy(mInstantiatedBldg);
	}

	// Update is called once per frame
	void LateUpdate()
    {
		if(mBuildingPlacementMode)
		{
			Vector3 patchOrigin = mTerrainGridScript.GetPatchPos();
			int nPatchesX = mTerrainGridScript.gridWidth / 2;
			int nPatchesZ = mTerrainGridScript.gridDepth / 2;
			Vector3 pos = patchOrigin + new Vector3(nPatchesX * mTerrainGridScript.cellSize, 0.0f, nPatchesZ * mTerrainGridScript.cellSize);

			mCurrentBuildingMeshRenderer.material = (mTerrainGridScript.CanPlace(0, 0) ? mPlacingMaterialValid : mPlacingMaterialInvalid);

        	UnitData data = UnitManager.GetUnitData(mCurrentBuilding);
        	data.lastPosition = mCurrentBuilding.transform.position;

        	UnitManager.ChangeUnit.Invoke(mCurrentBuilding, data);
        	mCurrentBuilding.transform.position = pos;

			if(Input.GetMouseButtonDown(0))
			{
				Destroy(mInstantiatedBldg);
				if(mTerrainGridScript.CanPlace(0, 0))
				{
					if (mFlattenOnPlacement)
					{
						// Flatten terrain if desired
						int heightmapWidth = mTerrain.terrainData.heightmapResolution;
						int heightmapDepth = mTerrain.terrainData.heightmapResolution;

						Vector3 relPos = (pos - mTerrain.gameObject.transform.position);
						Vector3 relCoords;
						relCoords.x = relPos.x / mTerrain.terrainData.size.x;
						relCoords.y = relPos.y / mTerrain.terrainData.size.y;
						relCoords.z = relPos.z / mTerrain.terrainData.size.z;

						int cellSize = (int)(mTerrainGridScript.cellSize) / 2;
						int u = (int)(relCoords.x * heightmapWidth);
						int v = (int)(relCoords.z * heightmapDepth);
						int offset = cellSize / 2;

						float[,] heights = mTerrain.terrainData.GetHeights(u - offset, v - offset, cellSize, cellSize);
						for (int i = 0; i < cellSize; i++)
						{
							for (int j = 0; j < cellSize; j++)
							{
								heights[i, j] = pos.y / mTerrainGenerator.heightScale;
							}
						}

						mTerrain.terrainData.SetHeights(u - offset, v - offset, heights);
					}

//					mCurrentBuildingMeshRenderer.material = mPlacedMaterial;
					GameObject placedBldg = Instantiate(mTestPrefab, pos, Quaternion.identity);
					placedBldg.GetComponent<MeshRenderer>().material = mPlacedMaterial;
					placedBldg.layer = LayerMask.NameToLayer("Buildings");

					mTestObjects.Add(placedBldg);

        	        BuildingData buildingData = new BuildingData
        	        {
        	            active = true,
        	        };

        	        BuildingManager.AddBuilding.Invoke(placedBldg, buildingData);
        	    }
			}
		}
	}
}
