﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SkyLightingScript : MonoBehaviour
{
	private Material mMaterial;
	public GameObject lightObject;

	[Range(0.0f, 4.0f)]
	public float rayleighBrightness = 2.527f;

	[Range(0.0f, 1.0f)]
	public float rayleighCollectionPower = 0.32591f;

	[Range(0.0f, 1.0f)]
	public float rayleighStrength = 0.256f;

	[Range(0.0f, 1.0f)]
	public float mieDistribution = 0.203f;

	[Range(0.0f, 5.0f)]
	public float mieBrightness = 1.741f;

	[Range(0.0f, 1.0f)]
	public float mieCollectionPower = 0.989f;

	[Range(0.0f, 1.0f)]
	public float mieStrength = 0.3827f;

	[Range(0.0f, 1.0f)]
	public float scatterStrength = 0.0803f;

	[Range(0.0f, 1.0f)]
	public float surfaceHeight = 0.9848f;

	private Vector3 mCurrentLightVector;
	private Vector3 mCurrentViewVector;

	public Vector3 GetLightVector()
	{
		return mCurrentLightVector;
	}

	public Vector3 GetViewVector()
	{
		return mCurrentViewVector;
	}

	private void Awake()
	{
		mMaterial = new Material(Shader.Find("Quadrion/SkyLightingShader"));
	}


	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Vector3 V = Camera.main.transform.forward;
		Vector3 L = lightObject.transform.forward;
		mCurrentLightVector = L;
		mCurrentViewVector = V;
		Matrix4x4 inverseV = Camera.main.worldToCameraMatrix.inverse;
		Matrix4x4 inverseP = Camera.main.projectionMatrix.inverse;
		mMaterial.SetVector("_V", V);
		mMaterial.SetVector("_L", L);
		mMaterial.SetFloat("_rayleighBrightness", rayleighBrightness);
		mMaterial.SetFloat("_rayleighCollectionPower", rayleighCollectionPower);
		mMaterial.SetFloat("_rayleighStrength", rayleighStrength);
		mMaterial.SetFloat("_mieDistribution", mieDistribution);
		mMaterial.SetFloat("_mieCollectionPower", mieCollectionPower);
		mMaterial.SetFloat("_mieStrength", mieStrength);
		mMaterial.SetFloat("_mieBrightness", mieBrightness);
		mMaterial.SetFloat("_scatterStrength", scatterStrength);
		mMaterial.SetFloat("_surfaceHeight", surfaceHeight);
		mMaterial.SetMatrix("_inverseV", inverseV);
		mMaterial.SetMatrix("_inverseP", inverseP);
		Graphics.Blit(source, destination, mMaterial);
	}

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}
}
