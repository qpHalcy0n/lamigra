﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

internal class UnitData
{
    internal bool active;
    internal bool hasChanged;
    internal Vector3 lastPosition;
	internal int lastFogCellX = 0;
	internal int lastFogCellZ = 0;

	internal void SetLastCell(int lastX, int lastZ)
	{
		lastFogCellX = lastX;
		lastFogCellZ = lastZ;
	}
}

internal class UnitAddEvent : UnityEvent<GameObject, UnitData> { }
internal class UnitRemovedEvent : UnityEvent<GameObject> { }
internal class UnitChangedEvent : UnityEvent<GameObject, UnitData> { }


public class UnitManager : MonoBehaviour
{
    #region Static Accessor
    internal static UnitAddEvent AddUnit;
    internal static UnitRemovedEvent RemoveUnit;
    internal static UnitChangedEvent ChangeUnit;
    #endregion

    #region Private Data
    private static readonly Dictionary<GameObject, UnitData> mUnits = new Dictionary<GameObject, UnitData>();
    #endregion

    #region Internal Functions
    internal void Awake()
    {
        AddUnit = new UnitAddEvent();
        AddUnit.AddListener(RegisterUnit);

        RemoveUnit = new UnitRemovedEvent();
        RemoveUnit.AddListener(DeregisterUnit);

        ChangeUnit = new UnitChangedEvent();
        ChangeUnit.AddListener(UpdateUnit);
    }

    internal static List<GameObject> GetUnits(bool aIsActive = true, bool aHasChanged = false)
    {
        List<GameObject> objects = new List<GameObject>();

        Dictionary<GameObject, UnitData> mUnitsCopy = new Dictionary<GameObject, UnitData>(mUnits);

        foreach (var obj in mUnitsCopy)
        {
            if (aHasChanged && obj.Value.hasChanged == false)
                continue;

            if (aIsActive && obj.Value.active == false)
                continue;

            if (aHasChanged && obj.Value.hasChanged)
            {
                UnitData data = obj.Value;
                data.hasChanged = false;
                mUnits[obj.Key] = data;
            }

            objects.Add(obj.Key);
        }

        return objects;
    }

    internal static Dictionary<GameObject, UnitData> GetUnitsAndData(bool aIsActive = true, bool aHasChanged = false)
    {
        Dictionary<GameObject, UnitData> objects = new Dictionary<GameObject, UnitData>();

        Dictionary<GameObject, UnitData> mUnitsCopy = new Dictionary<GameObject, UnitData>(mUnits);

        foreach (var obj in mUnitsCopy)
        {
            if (aHasChanged && obj.Value.hasChanged == false)
                continue;

            if (aIsActive && obj.Value.active == false)
                continue;

            if (aHasChanged && obj.Value.hasChanged)
            {
                UnitData data = obj.Value;
                data.hasChanged = false;
                mUnits[obj.Key] = data;
            }

            objects.Add(obj.Key, obj.Value);
        }

        return objects;
    }

    internal static UnitData GetUnitData(GameObject aObject)
    {
        return mUnits[aObject];
    }

    internal void RegisterUnit(GameObject aObject, UnitData aData)
    {
        mUnits.Add(aObject, aData);
    }

    internal void DeregisterUnit(GameObject aObject)
    {
        mUnits.Remove(aObject);
    }

    internal void UpdateUnit(GameObject aObject, UnitData aData)
    {
        aData.hasChanged = true;
        mUnits[aObject] = aData;
    }

    #endregion
}