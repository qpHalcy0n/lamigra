﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCamera : MonoBehaviour
{
	[Range(0.0f, 2.0f)]
	public float rotationSpeed				= 0.01f;

	[Range(0.0f, 2.0f)]
	public float elevationSpeed				= 0.01f;

	[Range(0.0f, 100.0f)]
	public float translationSpeed			= 0.01f;

	[Range(0.0f, 100.0f)]
	public float zoomSpeed					= 10.0f;

	[Range(0.5f, 10.0f)]
	public float colliderRadius				= 5.0f;


	private float mRotationAngle			= 0.0f;
	private Vector3 mFocusPoint				= new Vector3(1000.0f, 0.0f, 1000.0f);
	private bool mSuspendCam				= false;
	private float elevationAngle			= 70.0f;
	private float lockDistance				= 500.0f;

	private float maxX						= float.MinValue;
	private float maxY						= float.MinValue;

	void UpdateData()
	{
		if (Input.GetKey(KeyCode.Tab))
		{
			mSuspendCam = !mSuspendCam;
			if (mSuspendCam)
			{
				Cursor.lockState = (mSuspendCam) ? CursorLockMode.None : CursorLockMode.Confined;
				return;
			}
		}

		// Translate mode w/ middle mouse
		if(Input.GetMouseButton(2))
		{
			Cursor.lockState = CursorLockMode.Locked;

			float dx = Input.GetAxisRaw("Mouse X");
			float dz = Input.GetAxisRaw("Mouse Y");

			if(dx > maxX)
				maxX = dx;
			if(dz > maxY)
				maxY = dz;

			Vector3 right = new Vector3(transform.right.x, 0.0f, transform.right.z);
			mFocusPoint += right.normalized * dx * translationSpeed;

			Vector3 forward = new Vector3(transform.forward.x, 0.0f, transform.forward.z);
			mFocusPoint += forward.normalized * dz * translationSpeed;
		}

		if(Input.GetMouseButtonUp(2))
		{
			Cursor.lockState = CursorLockMode.None;
		}

		// Check for RMB hold for orbit cam
		if(Input.GetMouseButton(1) && Input.GetKey(KeyCode.LeftAlt))
		{
			Cursor.lockState = CursorLockMode.Locked;
		
			float dx = Input.GetAxis("Mouse X");
			float dy = Input.GetAxis("Mouse Y");

			elevationAngle += dy;
			if(elevationAngle >= 90.0f)
				elevationAngle = 89.9f;
			if(elevationAngle <= 0.0f)
				elevationAngle = 0.0f;

			mRotationAngle += dx * rotationSpeed;
			if(mRotationAngle >= 360.0f)
				mRotationAngle = 0.0f;
			if(mRotationAngle < 0.0f)
				mRotationAngle = 360.0f;
		}

		if(Input.GetMouseButtonUp(1))
		{
			Cursor.lockState = CursorLockMode.None;
		}

		lockDistance -= Input.mouseScrollDelta.y * zoomSpeed;
	}

	void CalculateCamera()
	{
		RaycastHit focusPointElevationHit;
		Vector3 focusPointOrigin = new Vector3(mFocusPoint.x, 5000.0f, mFocusPoint.z);
		Physics.Raycast(focusPointOrigin, new Vector3(0.0f, -1.0f, 0.0f), out focusPointElevationHit, Mathf.Infinity, LayerMask.GetMask("Terrain"));
		float focusPointElevation = focusPointElevationHit.point.y;

		float radius = lockDistance * Mathf.Cos(elevationAngle * Mathf.Deg2Rad);
		float camX = focusPointOrigin.x + (radius * Mathf.Cos(mRotationAngle * Mathf.Deg2Rad));
		float camZ = focusPointOrigin.z + (radius * Mathf.Sin(mRotationAngle * Mathf.Deg2Rad));

		RaycastHit cameraTerrainElevationHit;
		Vector3 cameraRayOrigin = new Vector3(camX, 5000.0f, camZ);
		Physics.Raycast(cameraRayOrigin, new Vector3(0.0f, -1.0f, 0.0f), out cameraTerrainElevationHit, Mathf.Infinity, LayerMask.GetMask("Terrain"));
		float cameraTerrainElevation = cameraTerrainElevationHit.point.y;

		float diskHeight = focusPointElevation + (lockDistance * Mathf.Sin(elevationAngle * Mathf.Deg2Rad));
		float finalHeight = Mathf.Max(diskHeight, cameraTerrainElevation + colliderRadius);

		Vector3 focusPoint = new Vector3(focusPointOrigin.x, focusPointElevation, focusPointOrigin.z);
		transform.position = new Vector3(camX, finalHeight, camZ);
		transform.forward = focusPoint - transform.position;
		transform.forward.Normalize();
	}

	// Start is called before the first frame update
	void Start()
    {
		Cursor.lockState = CursorLockMode.Confined;
		CalculateCamera();
	}

    // Update is called once per frame
    void Update()
    {
		UpdateData();
		CalculateCamera();	
    }

	private void OnPostRender()
	{
	
	}
}
