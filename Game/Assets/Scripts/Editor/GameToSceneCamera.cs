﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

class GameToSceneCamera : ScriptableWizard
{
    [MenuItem("Camera/Set Game To Scene")]
    static void GameToScene()
    {
        Camera scene = SceneView.lastActiveSceneView.camera;
        Camera cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        if (scene == null)
        {
            Debug.Log("Could not find scene camera!");
            return;
        }

        if (cam == null)
        {
            Debug.Log("No camera in scene with the MainCamera tag");
            return;
        }

        cam.transform.forward = scene.transform.forward;
        cam.transform.position = scene.transform.position;
    }

    [MenuItem("Camera/Set Scene To Game")]
    private static void SceneToGame()
    {
        SceneView scene = SceneView.lastActiveSceneView;
        Camera cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        if (scene == null)
        {
            Debug.Log("Could not find scene camera!");
            return;
        }

        if (cam == null)
        {
            Debug.Log("No camera in scene with the MainCamera tag");
            return;
        }

        scene.rotation = Quaternion.LookRotation(cam.transform.forward);
        scene.pivot = cam.transform.position;

        SceneView.lastActiveSceneView.Repaint();
    }
}
