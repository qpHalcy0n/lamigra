﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(FFTTerrainGenerator))]
public class TerrainInspector : Editor
{
	public override void OnInspectorGUI()
	{
		base.DrawDefaultInspector();

		if (GUILayout.Button("Generate"))
		{
			GameObject.FindObjectOfType<FFTTerrainGenerator>().Generate();
		}
	}
}
