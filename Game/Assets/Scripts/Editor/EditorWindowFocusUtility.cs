﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EditorWindowFocusUtility
{

    static EditorWindowFocusUtility()
    {
        EditorApplication.update += Update;    
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private static void Update()
    {
        if(!UnityEditorInternal.InternalEditorUtility.isApplicationActive)
            Cursor.lockState = CursorLockMode.None;
    }
}
