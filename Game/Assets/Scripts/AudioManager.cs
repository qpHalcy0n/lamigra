﻿using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    [RequireComponent(typeof(AudioListener))]
    public class AudioManager : MonoBehaviour
    {
        #region Private Data

        private static List<AudioSource> mSFXAudioSources;
        private static List<AudioSource> mMusicAudioSources;

        private const int LIST_CLEAN_TIME = 60;
        private int mCleanLists = LIST_CLEAN_TIME;

        #endregion

        #region Properties

        public static bool MuteAll { get; set; }
        public static bool MuteSFX { get; set; }
        public static bool MuteMusic { get; set; }
        public static AudioManager Instance { get; private set; }

        #endregion

        #region Events

        protected void Awake()
        {
            mSFXAudioSources = new List<AudioSource>();
            mMusicAudioSources = new List<AudioSource>();

            Instance = this;
        }

        protected void Update()
        {
            MuteMusicSources(MuteAll || MuteMusic);
            MuteSFXSources(MuteAll || MuteSFX);

            --mCleanLists;
            if (mCleanLists > 0) return;

            mCleanLists = LIST_CLEAN_TIME;
            CleanAudioSourcesList();
        }

        #endregion

        #region Public API

        public static AudioSource PlaySFX(string aSFXName, float aVolume = 1.0f, int aLoop = 0,
            GameObject aParent = null)
        {
            GameObject bindTo = aParent;
            if (bindTo == null) bindTo = Instance.gameObject;

            AudioClip clip = Resources.Load<AudioClip>(aSFXName);
            if (clip == null)
            {
                Debug.LogError("SFX not found: " + aSFXName);
                return null;
            }

            AudioSource source = bindTo.AddComponent<AudioSource>();
            source.priority = 10;
            source.clip = clip;
            source.volume = aVolume;
            source.minDistance = 50.0f;
            source.dopplerLevel = 0.0f;
            source.mute = MuteAll || MuteSFX;

            if (aLoop != 0)
            {
                source.loop = true;
            }

            if (aLoop >= 0)
            {
                Destroy(source, clip.length * (aLoop + 1));
            }

            source.Play();

            mSFXAudioSources.Add(source);

            return source;
        }

        public static AudioSource PlayMusic(string aMusicName, float aVolume = 1.0f, int aLoop = 0,
            GameObject aParent = null)
        {
            GameObject bindTo = aParent;
            if (bindTo == null) bindTo = Instance.gameObject;

            AudioClip clip = Resources.Load<AudioClip>(aMusicName);
            if (clip == null)
            {
                Debug.LogError("Music not found: " + aMusicName);
                return null;
            }

            AudioSource source = bindTo.AddComponent<AudioSource>();
            source.priority = 1;
            source.clip = clip;
            source.volume = aVolume;
            source.mute = MuteAll || MuteSFX;

            if (aLoop != 0)
            {
                source.loop = true;
            }

            if (aLoop >= 0)
            {
                Destroy(source, clip.length * (aLoop + 1));
            }

            source.Play();

            mMusicAudioSources.Add(source);

            return source;
        }

        public static AudioSource GetSourceWithName(string aName)
        {
            int count = mMusicAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                if (mMusicAudioSources[i].name == aName) return mMusicAudioSources[i];
            }

            count = mSFXAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                if (mSFXAudioSources[i].name == aName) return mSFXAudioSources[i];
            }

            return null;
        } 

        public static void SetMusicVolume(float aVolume)
        {
            int count = mMusicAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                AudioSource audioSource = mMusicAudioSources[i];
                if (audioSource != null)
                {
                    audioSource.volume = aVolume;
                }
            }
        }

        public static void SetSFXVolume(float aVolume)
        {
            int count = mSFXAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                AudioSource audioSource = mSFXAudioSources[i];
                if (audioSource != null)
                {
                    audioSource.volume = aVolume;
                }
            }
        }

        public static void StopAllSounds()
        {
            StopAllMusic();
            StopAllSFX();
        }

        public static void StopAllMusic()
        {
            int count = mMusicAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                AudioSource audioSource = mMusicAudioSources[i];
                if (audioSource != null)
                {
                    Destroy(audioSource);
                }

                mMusicAudioSources.RemoveAt(i);
            }
        }

        public static void StopAllSFX()
        {
            int count = mSFXAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                AudioSource audioSource = mSFXAudioSources[i];
                if (audioSource != null)
                {
                    Destroy(audioSource);
                }

                mSFXAudioSources.RemoveAt(i);
            }
        }

        #endregion

        #region Private API

        private static void CleanAudioSourcesList()
        {
            int count = mSFXAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                AudioSource audioSource = mSFXAudioSources[i];
                if (audioSource == null)
                {
                    mSFXAudioSources.RemoveAt(i);
                }
            }

            count = mMusicAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                AudioSource audioSource = mMusicAudioSources[i];
                if (audioSource == null)
                {
                    mMusicAudioSources.RemoveAt(i);
                }
            }
        }

        private void MuteSFXSources(bool aMuted)
        {
            int count = mSFXAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                AudioSource audioSource = mSFXAudioSources[i];
                if (audioSource != null)
                {
                    audioSource.mute = aMuted;
                }
                else
                {
                    mSFXAudioSources.RemoveAt(i);
                }
            }
        }

        private void MuteMusicSources(bool aMuted)
        {
            int count = mMusicAudioSources.Count;
            for (int i = count - 1; i >= 0; --i)
            {
                AudioSource audioSource = mMusicAudioSources[i];
                if (audioSource != null)
                {
                    audioSource.mute = aMuted;
                }
                else
                {
                    mMusicAudioSources.RemoveAt(i);
                }
            }
        }

        #endregion
    }
}