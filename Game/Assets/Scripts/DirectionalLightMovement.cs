﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalLightMovement : MonoBehaviour
{
	//    public Vector3 lightVector;

	[Range(-1.0f, 1.0f)]
	public float lightVectorX;

	[Range(-1.0f, 1.0f)]
	public float lightVectorY;

	[Range(-1.0f, 1.0f)]
	public float lightVectorZ;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		transform.forward = new Vector3(lightVectorX, lightVectorY, lightVectorZ);
	}
}
