﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGridRenderer : MonoBehaviour
{
	public float cellSize					= 10.0f;		// world units
	public int	 gridWidth					= 6;		// in cells
	public int	 gridDepth					= 6;		// in cells
	public float yOffset					= 0.5f;		// to fudge the overdraw
	public float steepnessCutoff			= 30.0f;
	public Material cellMaterialValid;
	public Material cellMaterialInvalid;

	private Terrain				mTerrain;
	private TerrainData			mTerrainData;
	private GameObject[]		mCells;
	private bool[]				mIsCellValid;
	private float[]				mHeights;
	private GameObject			mParentGameObj;
	private Vector3				mPatchPos;

	Mesh CreateMesh()
	{
		Mesh mesh = new Mesh();
		mesh.name = "Grid Cell";
		mesh.vertices = new Vector3[] { Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero };
		mesh.triangles = new int[] { 0, 1, 2, 2, 1, 3 };
		mesh.normals = new Vector3[] { Vector3.up, Vector3.up, Vector3.up, Vector3.up };
		mesh.uv = new Vector2[] { new Vector2(1, 1), new Vector2(1, 0), new Vector2(0, 1), new Vector2(0, 0) };
		mesh.bounds = new Bounds(Vector3.zero, new Vector3(99999.9f, 99999.9f, 99999.9f));
		
		return mesh;
	}

	GameObject CreateChild()
	{
		GameObject go = new GameObject();

		go.name = "Grid Cell";
		go.transform.parent = transform;
		go.transform.localPosition = Vector3.zero;
		go.AddComponent<MeshRenderer>();
		go.AddComponent<MeshFilter>().mesh = CreateMesh();

		return go;
	}

    // Start is called before the first frame update
    void Start()
    {
		mTerrain = GameObject.FindObjectOfType<Terrain>().GetComponent<Terrain>();
		mTerrainData = mTerrain.terrainData;
		
//		FFTTerrainGenerator shit = gameObject.GetComponentInParent<FFTTerrainGenerator>(); 

		mCells = new GameObject[gridWidth * gridDepth];
		mIsCellValid = new bool[gridWidth * gridDepth];
		mHeights = new float[(gridWidth + 1) * (gridDepth + 1)];

		for(int z = 0; z < gridDepth; z++)
		{
			for(int x = 0; x < gridWidth; x++)
			{
				mCells[z * gridWidth + x] = CreateChild();
				mIsCellValid[z * gridWidth + x] = true;
			}
		}
    }

    // Update is called once per frame
    void Update()
    {
		for(int i = 0; i < gridWidth * gridDepth; i++)
			mIsCellValid[i] = true;

		UpdateSize();
		UpdatePosition();
		UpdateHeights();
		UpdateCells();
    }

	void UpdateSize()
	{
		int newSize = gridDepth * gridDepth;
		int oldSize = mCells.Length;

		if(newSize == oldSize)
			return;

		GameObject[] oldCells = mCells;
		bool[] oldIsCellValid = mIsCellValid;
		mCells = new GameObject[newSize];
		mIsCellValid = new bool[newSize];

		if(newSize < oldSize)
		{
			for (int i = 0; i < newSize; i++)
			{
				mCells[i] = oldCells[i];
				mIsCellValid[i] = oldIsCellValid[i];
			}

			for (int i = newSize; i < oldSize; i++)
			{
				Destroy(oldCells[i]);
			}
		}

		else if(newSize > oldSize)
		{
			for (int i = 0; i < oldSize; i++)
			{
				mCells[i] = oldCells[i];
				mIsCellValid[i] = oldIsCellValid[i];
			}

			for (int i = oldSize; i < newSize; i++)
			{
				mCells[i] = CreateChild();
				mIsCellValid[i] = true;
			}
		}

		mHeights = new float[(gridDepth + 1) * (gridWidth + 1)];
	}

	void UpdatePosition()
	{
		RaycastHit hitInfo;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Physics.Raycast(ray, out hitInfo, Mathf.Infinity, LayerMask.GetMask("Terrain"));
		
		Vector3 pos = hitInfo.point;
		pos.x -= hitInfo.point.x % cellSize + gridWidth * cellSize / 2.0f;
		pos.z -= hitInfo.point.z % cellSize + gridDepth * cellSize / 2.0f;
		pos.y = 0;

		transform.localPosition = pos;
		mPatchPos = pos;
	}

	public Vector3 GetPatchPos()
	{
		return mPatchPos;
	}

	void UpdateHeights()
	{
		RaycastHit hitInfo;
		Vector3 origin;

		for(int z = 0; z < gridDepth + 1; z++)
		{
			for(int x = 0; x < gridWidth + 1; x++)
			{
				origin = new Vector3(x * cellSize, 5000, z * cellSize);			// may have to fudge Y
				Physics.Raycast(transform.TransformPoint(origin), Vector3.down, out hitInfo, Mathf.Infinity, LayerMask.GetMask("Terrain"));
				mHeights[z * (gridWidth + 1) + x] = hitInfo.point.y;

				if(z == gridDepth / 2 && x == gridWidth / 2)
					mPatchPos.y = hitInfo.point.y;
			}
		}
	}

	Vector3 MeshVertex(int x, int z)
	{
		return new Vector3(x * cellSize, mHeights[z * (gridWidth + 1) + x] + yOffset, z * cellSize);
	}

	void UpdateMesh(Mesh mesh, int x, int z)
	{
		mesh.vertices = new Vector3[]
		{
			MeshVertex(x, z),
			MeshVertex(x, z + 1),
			MeshVertex(x + 1, z),
			MeshVertex(x + 1, z + 1),
		};
	}

	bool IsCellValid(int x, int z)
	{
		RaycastHit hitInfo, terrainHitInfo;
		Vector3 origin = new Vector3(x * cellSize + cellSize / 2.0f, 5000, z * cellSize + cellSize / 2.0f);			// may have to be fudged in y

		Vector3 originVec = transform.TransformPoint(origin);

		if (Physics.Raycast(transform.TransformPoint(origin), Vector3.down, out hitInfo, Mathf.Infinity, LayerMask.GetMask("Water")))
		{
			Physics.Raycast(transform.TransformPoint(origin), Vector3.down, out terrainHitInfo, Mathf.Infinity, LayerMask.GetMask("Terrain"));
			if(hitInfo.distance < terrainHitInfo.distance)
				return false;
		}

		Physics.Raycast(transform.TransformPoint(origin), Vector3.down, out hitInfo, Mathf.Infinity, LayerMask.GetMask("Buildings"));

		bool steepnessCheck = mIsCellValid[z * gridWidth + x];

		return (hitInfo.collider == null && steepnessCheck);
	}

	public bool CanPlace(int width, int depth)
	{
		bool even = (gridWidth % 2 == 0);

		int left = (gridWidth / 2) - 1;
		int right = left + 1;
		int top = (gridDepth / 2) - 1;
		int bottom = top + 1;

		return IsCellValid(left, top) && IsCellValid(right, top) && IsCellValid(left, bottom) && IsCellValid(right, bottom);
	}

	void UpdateCells()
	{
		for(int z = 0; z < gridDepth; z++)
		{
			for(int x = 0; x < gridWidth; x++)
			{
				GameObject cell = mCells[z * gridWidth + x];
				MeshRenderer meshRenderer = cell.GetComponent<MeshRenderer>();
				MeshFilter meshFilter = cell.GetComponent<MeshFilter>();

				Vector3 samplePos = transform.position + new Vector3(x * cellSize, 0.0f, z * cellSize);
				Vector3 terrainCenter = mTerrain.terrainData.bounds.center;
				Vector3 terrainSpan = mTerrain.terrainData.size;
				Vector3 terrainMins = terrainCenter - terrainSpan * 0.5f;

				float u = (samplePos.x - terrainMins.x) / terrainSpan.x;
				float v = (samplePos.z - terrainMins.z) / terrainSpan.z;
				float gradSteep = mTerrainData.GetSteepness(u, v);

				if (gradSteep > steepnessCutoff)
					mIsCellValid[z * gridWidth + x] = false;
				
				meshRenderer.material = IsCellValid(x, z) ? cellMaterialValid : cellMaterialInvalid;
				UpdateMesh(meshFilter.mesh, x, z);
			}
		}
	}
}
