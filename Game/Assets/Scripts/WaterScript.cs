﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterScript : MonoBehaviour
{
	public Texture				reflectionTexture;
	public Texture				waterNormalmap;
	public Texture				foamTexture;
	public GameObject			lightObject;
	public float				waterElevation;

	[Range(0.0f, 0.005f)]
	public float				refractionScale;

	[Range(0.0f, 1.0f)]
	public float				distortion, radiance;

	[Range(0.0f, 1.0f)]
	public float				clarity, transparency;

	public Vector4				foamRanges;
	public Vector3				horizontalExtinction;
	public Vector3				shoreTint;
	public Vector3				surfaceColor;
	public Vector3				deepColor;


	private Vector2				mGerstnerDirection0 = new Vector2(0.772f, -0.428f);
	private Vector2				mGerstnerDirection1 = new Vector2(0.534f, 0.136f);
	private Vector2				mGerstnerDirection2 = new Vector2(-0.883f, -0.208f);
	private Vector2				mGerstnerDirection3 = new Vector2(-0.748f, 0.498f);
	private Vector2				mGerstnerDirection4 = new Vector2(-0.38f, 0.127f);

	private float				mGerstnerAmplitude0 = 3.1f;
	private float				mGerstnerAmplitude1 = 2.8f;
	private float				mGerstnerAmplitude2 = 2.3f;
	private float				mGerstnerAmplitude3 = 1.0f;
	private float				mGerstnerAmplitude4 = 1.4f;

	private float				mGerstnerSteepness0 = 4.03f;
	private float				mGerstnerSteepness1 = 2.94f;
	private float				mGerstnerSteepness2 = 5.25f;
	private float				mGerstnerSteepness3 = 4.56f;
	private float				mGerstnerSteepness4 = 4.66f;

	private float				mGerstnerFrequency0 = 0.0218f;
	private float				mGerstnerFrequency1 = 0.0245f;
	private float				mGerstnerFrequency2 = 0.0091f;
	private float				mGerstnerFrequency3 = 0.0313f;
	private float				mGerstnerFrequency4 = 0.0466f;

	private float				mGerstnerSpeed0 = 3.04f;
	private float				mGerstnerSpeed1 = 1.72f;
	private float				mGerstnerSpeed2 = 0.67f;
	private float				mGerstnerSpeed3 = 1.08f;
	private float				mGerstnerSpeed4 = 1.77f;


	private SunDirectionScript					mSunDirectionScript;
	private FFTTerrainGenerator					mTerrainGenerator;
	private Material							mMaterial;
	private ReflectionCamScript					mReflectionCamScript;
	private Vector4								mTerrainSpan;
	private float								dt;
	private Terrain								mTerrain;
	public RenderTexture						mTerrainHeightmap;
	private float								mMaxTerrainElevation;
	private Vector4[] mGerstnerDirections	= new Vector4[5];
	private float[] mGerstnerAmplitudes		= new float[5];
	private float[] mGerstnerSteepnesses	= new float[5];
	private float[] mGerstnerFrequencies	= new float[5];
	private float[] mGerstnerSpeeds			= new float[5];
	private int mNumGerstnerWaves			= 5;

	// Start is called before the first frame update
	void Start()
    {
		mTerrainGenerator = gameObject.transform.parent.GetComponentInChildren<FFTTerrainGenerator>();
		Vector3 terrainMins = mTerrainGenerator.GetTerrainMins();
		Vector3 terrainSpan = mTerrainGenerator.GetTerrainSpan();

		terrainSpan.y = 0.0f;
		terrainMins.y = waterElevation;
		mTerrain = mTerrainGenerator.GetTerrain();
		mTerrainHeightmap = mTerrain.terrainData.heightmapTexture;
//		mMaxTerrainElevation = mTerrain.terrainData.heightmapScale.y;
		mMaxTerrainElevation = mTerrain.terrainData.size.y;
		mTerrainSpan.Set(terrainSpan.x, terrainSpan.y, terrainSpan.z, mMaxTerrainElevation);

		Vector3[] norms = { new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f),
							new Vector3(0.0f, 1.0f, 0.0f), new Vector3(0.0f, 1.0f, 0.0f)};
		Vector3[] verts = { terrainMins, new Vector3(terrainMins.x + terrainSpan.x, terrainMins.y, terrainMins.z),
						    terrainMins + terrainSpan, new Vector3(terrainMins.x, terrainMins.y, terrainMins.z + terrainSpan.z)};
		int[] triangles = { 3, 1, 0, 3, 2, 1 };

		Mesh mesh = new Mesh();
		mesh.vertices = verts;
		mesh.normals = norms;
		mesh.triangles = triangles;
		MeshHelper.Subdivide(mesh, 48);
		mesh.Optimize();

		GetComponent<MeshFilter>().mesh = mesh;	
		mMaterial = GetComponent<MeshRenderer>().material;
		mReflectionCamScript = GameObject.FindObjectOfType<ReflectionCamScript>();
		mReflectionCamScript.SetWaterElevation(waterElevation);
		dt = 0.0f;

		mSunDirectionScript = GameObject.FindObjectOfType<SunDirectionScript>();
	}

	private void PopulateGerstnerArrays()
	{
		mGerstnerDirections[0] = new Vector4(mGerstnerDirection0.x, 0.0f, mGerstnerDirection0.y, 0.0f);
		mGerstnerDirections[1] = new Vector4(mGerstnerDirection1.x, 0.0f, mGerstnerDirection1.y, 0.0f);
		mGerstnerDirections[2] = new Vector4(mGerstnerDirection2.x, 0.0f, mGerstnerDirection2.y, 0.0f);
		mGerstnerDirections[3] = new Vector4(mGerstnerDirection3.x, 0.0f, mGerstnerDirection3.y, 0.0f);
		mGerstnerDirections[4] = new Vector4(mGerstnerDirection4.x, 0.0f, mGerstnerDirection4.y, 0.0f);

		mGerstnerAmplitudes[0] = mGerstnerAmplitude0;
		mGerstnerAmplitudes[1] = mGerstnerAmplitude1;
		mGerstnerAmplitudes[2] = mGerstnerAmplitude2;
		mGerstnerAmplitudes[3] = mGerstnerAmplitude3;
		mGerstnerAmplitudes[4] = mGerstnerAmplitude4;

		mGerstnerFrequencies[0] = mGerstnerFrequency0;
		mGerstnerFrequencies[1] = mGerstnerFrequency1;
		mGerstnerFrequencies[2] = mGerstnerFrequency2;
		mGerstnerFrequencies[3] = mGerstnerFrequency3;
		mGerstnerFrequencies[4] = mGerstnerFrequency4;

		mGerstnerSpeeds[0] = mGerstnerSpeed0;
		mGerstnerSpeeds[1] = mGerstnerSpeed1;
		mGerstnerSpeeds[2] = mGerstnerSpeed2;
		mGerstnerSpeeds[3] = mGerstnerSpeed3;
		mGerstnerSpeeds[4] = mGerstnerSpeed4;

		mGerstnerSteepnesses[0] = mGerstnerSteepness0;
		mGerstnerSteepnesses[1] = mGerstnerSteepness1;
		mGerstnerSteepnesses[2] = mGerstnerSteepness2;
		mGerstnerSteepnesses[3] = mGerstnerSteepness3;
		mGerstnerSteepnesses[4] = mGerstnerSteepness4;
	}

    // Update is called once per frame
    void Update()
    {
		dt += Time.deltaTime;
		if (dt >= float.MaxValue - 10.0f)
			dt = 0.0f;

		PopulateGerstnerArrays();
		mReflectionCamScript.SetWaterElevation(waterElevation);
	}

	private void OnRenderObject()
	{
		mTerrainHeightmap = mTerrain.terrainData.heightmapTexture;
		Matrix4x4 reflectedMVP;
		Matrix4x4 reflectedV = mReflectionCamScript.GetReflectionViewMatrix();
		Matrix4x4 reflectedP = mReflectionCamScript.GetReflectionProjectionMatrix();
		reflectedMVP = reflectedP * reflectedV;
		Vector4 waveParams = new Vector4(0.0f, dt, 0.0f, waterElevation);

		float farClip = Camera.main.farClipPlane;
		float fov = Camera.main.fieldOfView;
		float aspect = Camera.main.aspect;
		float halfHeight = Camera.main.farClipPlane * Mathf.Tan(Mathf.Deg2Rad * (fov * 0.5f));
		float halfWidth = halfHeight * aspect;
		Vector4 UL = new Vector4(-halfWidth, halfHeight, farClip, 0.0f);
		Vector4 UR = new Vector4(halfWidth, halfHeight, farClip, 0.0f);
		Vector4 LR = new Vector4(halfWidth, -halfHeight, farClip, 0.0f);
		Vector4 LL = new Vector4(-halfWidth, -halfHeight, farClip, 0.0f);
		Matrix4x4 inverseV = Camera.main.worldToCameraMatrix.inverse;

		Vector3 L = mSunDirectionScript.GetLightVector();
		Vector2 clarityParams = new Vector2(clarity, transparency);

		mMaterial.SetTexture("_ReflectionMap", reflectionTexture);	
		mMaterial.SetTexture("_TerrainHeightmap", mTerrainHeightmap);
		mMaterial.SetTexture("_waterNormalmap", waterNormalmap);
		mMaterial.SetTexture("_foamTexture", foamTexture);
		mMaterial.SetMatrix("_reflCamMVP", reflectedMVP);
		mMaterial.SetVector("_waveParams", waveParams);
		mMaterial.SetVector("_meshSpan", mTerrainSpan);
		mMaterial.SetVector("_frustumUL", UL);
		mMaterial.SetVector("_frustumUR", UR);
		mMaterial.SetVector("_frustumLR", LR);
		mMaterial.SetVector("_frustumLL", LL);
		mMaterial.SetMatrix("_inverseV", inverseV);
		mMaterial.SetFloat("_refractionScale", refractionScale);
		mMaterial.SetFloat("_distortion", distortion);
		mMaterial.SetFloat("_radiance", radiance);
		mMaterial.SetVector("_camPosWS", Camera.main.transform.position);
		mMaterial.SetVectorArray("_gerstner_direction", mGerstnerDirections);
		mMaterial.SetFloatArray("_gerstner_amplitude", mGerstnerAmplitudes);
		mMaterial.SetFloatArray("_gerstner_steepness", mGerstnerSteepnesses);
		mMaterial.SetFloatArray("_gerstner_frequency", mGerstnerFrequencies);
		mMaterial.SetFloatArray("_gerstner_speed", mGerstnerSpeeds);
		mMaterial.SetInt("_num_gerstner_waves", mNumGerstnerWaves);
		mMaterial.SetVector("_lightDirWS", L);
		mMaterial.SetVector("_clarityParams", clarityParams);
		mMaterial.SetVector("_foamRanges", foamRanges);
		mMaterial.SetVector("_horizontalExtinction", horizontalExtinction);
		mMaterial.SetVector("_shoreTint", shoreTint);
		mMaterial.SetVector("_surfaceColor", surfaceColor);
		mMaterial.SetVector("_deepColor", deepColor);
	}
}
